/**
 * @typedef CommonNotice
 * @property {string} nom
 * @property {string} date_creation
 * @property {string} date_modification
 * @property { {src: string}? } logo
 * @property {string} site_web
 * @property {string[]} plateformes
 * @property {string[]} langues
 * @property {string} description_courte
 * @property {string} createurices
 * @property {string} alternative_a
 * @property {string[]} licences
 * @property {string} lien_wikipedia
 * @property {string} lien_exodus
 * @property {string} identifiant_wikidata
 * @property {string} description_longue
 * @property {string} [redirect_from]
 */

/**
 * @typedef {CommonNotice & {mis_en_avant: boolean, tags: Set<string>}} Notice
 */

/**
 * @typedef {CommonNotice & {mis_en_avant: 'oui' | 'non', tags: string[]}} YAMLNotice
 */

/**
 * @typedef JekyllNoticeSupplement
 * @property {string} url
 * @property {string} id
 * @property {string} path
 * @property {string} raw_content
 */

/**
 * @typedef { YAMLNotice & JekyllNoticeSupplement } JekyllNotice 
 */