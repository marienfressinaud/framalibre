---
nom: "Minitube"
date_creation: "Samedi, 6 juillet, 2019 - 10:50"
date_modification: "Samedi, 6 mai, 2023 - 10:18"
logo:
    src: "images/logo/Minitube.png"
site_web: "https://flavio.tordini.org/minitube"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Client léger pour Youtube, multiplateforme.
La version démonstration est gratuite"
createurices: "Flavio Tordini"
alternative_a: "Youtube"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "lecteur vidéo"
    - "client youtube"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/minitube"
---

Minitube est un client léger pour YouTube libre et multiplateforme qui permet de rechercher et visionner les vidéos du site, mais aussi de s'abonner à des chaînes sans être inscrit sur le service.
Plutôt que d'imiter le site web, minitube se veut plus proche  de l'expérience  télévisuelle : cherchez un mot clé et les vidéos en rapport s’enchainent sans interruption, ni commentaire, ni annotation. Vous pouvez regarder ce qui vous intéresse, en toute tranquillité, tout cela avec une consommation de mémoire vive et de processeur bien moindre qu’un navigateur, idéal pour les configurations modestes.

