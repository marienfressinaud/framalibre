---
nom: "XCas"
date_creation: "Samedi, 19 août, 2017 - 22:20"
date_modification: "Samedi, 19 août, 2017 - 22:26"
logo:
    src: "images/logo/XCas.png"
site_web: "http://www-fourier.ujf-grenoble.fr/~parisse/giac_fr.html"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Giac/Xcas est un système de calcul formel libre pour Windows, Mac OSX et Linux/Unix (licence GPL3)"
createurices: "Bernard PARISSE"
alternative_a: "maxima, Sage"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "calcul formel"
    - "géométrie"
    - "mathématiques"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Xcas"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/xcas"
---

Xcas est une interface de Giac, qui permet de faire du calcul formel, des représentations graphiques dans le plan ou l'espace, de la géométrie dynamique (dans le plan ou dans l'espace), du tableur, des statistiques et de la programmation.
Giac, est le moteur de calcul de Xcas, il s'agit d'une bibliothèque C++. On peut l'utiliser dans des programmes C++ mais aussi depuis Python, Java et Javascript.
Giac/Xcas dispose d'un mode de compatibilité pour les personnes habituées aux logiciels de calcul formel Maple ou MuPAD, ainsi que pour les utilisateurs de calculatrices TI.
Fonctionnalités de Xcas :
- calcul formel: arithmétique, intégration, dérivation, limites, ...
- graphes de fonction, paramétrique, dans le plan et l'espace
- tableur et statistiques: calcul approché et exact
- géométrie interactive: dans le plan et l'espace
- programmation
- tortue logo
- aide en ligne : documentation entièrement en français, index des fonctions, syntaxe, exemples, recherche par mot clef, ...

