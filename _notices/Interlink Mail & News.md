---
nom: "Interlink Mail & News"
date_creation: "Samedi, 6 juin, 2020 - 16:45"
date_modification: "Vendredi, 13 août, 2021 - 18:00"
logo:
    src: "images/logo/Interlink Mail & News.png"
site_web: "https://binaryoutcast.com/projects/interlink/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "English"
description_courte: "Un client mail performant et personnalisable, avec une interface « classique »."
createurices: "Matt A. Tobin"
alternative_a: "Windows Mail, Microsoft Outlook, IncrediMail, Application Courrier (Windows 10)"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "internet"
    - "client mail"
    - "lecteur de flux rss"
    - "agenda"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/interlink-mail-news"
---

Interlink Mail & News est un client mail (POP3 et IMAP) avec une interface « classique » basé sur le code Thunderbird. Il en reprend la plupart des fonctionnalités (flux RSS, accès aux groupes de discussions USENET, intégration d'agenda en ligne ou enregistré sur l'ordinateur).
Ce logiciel de communication a aussi son propre catalogue d'extensions pour ajouter des fonctionnalités (comme l'accès aux salons de discussion instantanée IRC) et des thèmes pour personnaliser l'apparence du logiciel.

