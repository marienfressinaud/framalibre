---
nom: "AmapJ"
date_creation: "Mercredi, 29 juillet, 2020 - 00:49"
date_modification: "Vendredi, 7 mai, 2021 - 11:02"
logo:
    src: "images/logo/AmapJ.png"
site_web: "https://amapj.fr/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
description_courte: "Logiciel de gestion des commandes au sein d'une Association de Maintien de l'Agriculture Paysanne"
createurices: "Emmanuel BRUN"
alternative_a: "La ruche qui dit oui"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métiers"
    - "amap"
    - "circuit court"
    - "agriculture"
    - "gestion"
    - "système de commande de nourriture en ligne"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/amapj"
---

Ce logiciel permet aux Association de Maintien de l'Agriculture Paysanne (AMAP) de gérer les commandes de produits passées par les membres de l'association. Il répond à différents besoins de différents acteurs : consommateur, référent, producteur, administrateur.

