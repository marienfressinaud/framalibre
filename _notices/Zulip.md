---
nom: "Zulip"
date_creation: "Jeudi, 8 juin, 2017 - 10:45"
date_modification: "Mercredi, 12 mai, 2021 - 16:20"
logo:
    src: "images/logo/Zulip.png"
site_web: "https://zulip.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "Une app pour tchater en groupe"
createurices: ""
alternative_a: "Slack"
licences:
    - "Licence Apache (Apache)"
tags:
    - "internet"
    - "messagerie instantanée"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/zulip"
---

Zulip est une application (web, bureautique ou Android) qui vise à combler les besoins de communication de groupes, notamment d'équipes de développeurs. Sur Zulip:
les conversations sont groupées,
on ne loupe pas les messages quand on est déconnecté,
on peut chercher dans l'historique des conversations,
on peut s'échanger des fichiers multimédias,
on peut partager du code et intégrer des services externes (Github, Gitlab, Gogs, Travis CI, flux RSS, Twitter, Slack,…)
et faire d'autres trucs encore.

