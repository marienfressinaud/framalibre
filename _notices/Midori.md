---
nom: "Midori"
date_creation: "Mercredi, 18 janvier, 2017 - 14:34"
date_modification: "Mercredi, 11 janvier, 2023 - 11:53"
logo:
    src: "images/logo/Midori.png"
site_web: "https://astian.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un navigateur léger, moderne et puissant, avec toutes les fonctionnalités pour une bonne navigation web."
createurices: ""
alternative_a: "Internet Explorer, Google Chrome, Chrome, Opera, Microsoft Edge"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "internet"
    - "navigateur web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Midori_(navigateur)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/midori"
---

Midori est un navigateur qui combine légèreté, puissance, et modernité. Il supporte tous les standards du W3C (HTML5, CSS3, EcmaScript 5) car il est basé sur WebKit, le moteur de rendu open-source, qui est entre autres utilisé par Safari (d'Apple) et anciennement par Google Chrome. Néanmoins, il est beaucoup moins gourmand en mémoire, et permet donc une utilisation sur des vieux ordinateurs ou lents. Il est disponible sous Windows et GNU/Linux ainsi que FreeBSD (et dérivés). Midori offre toutes les fonctionnalités de base d'un navigateur, c'est-à-dire gestion des signets, navigation privée, historique, gestion des cookies, plug-ins et personnalisation légère (ex : changement du moteur de recherche, de la police d'affichage, etc.).

