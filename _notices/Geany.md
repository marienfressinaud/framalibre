---
nom: "Geany"
date_creation: "Dimanche, 16 avril, 2017 - 13:38"
date_modification: "Jeudi, 19 octobre, 2023 - 22:45"
logo:
    src: "images/logo/Geany.png"
site_web: "http://www.geany.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Environnement de développement multiplate-formes léger et modulaire."
createurices: ""
alternative_a: "Notepad, Sublime Text, VSCode"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "ide"
    - "traitement de texte"
    - "environnement de développement"
lien_wikipedia: "https://en.wikipedia.org/wiki/Geany"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/geany"
---

Geany est un IDE multiplate-formes léger et modulaire. Il intègre la coloration syntaxique, l'auto-complétion, l'ouverture des fichiers par onglets (via projets ou indépendants), une console et la gestion de la compilation avec raccourcit clavier.
Les plugins sont nombreux et ajoutent (entre-autres) : affichage des l'arborescence des dossiers, correction orthographique, gestion des tâches, l'aperçu en temps réel pour l'édition MarkDown et HTML, macros, ...
Il est aussi très facile à adapter à son usage, via les préférences de l'application ou directement dans les fichiers de configuration (simples fichiers textes décrits dans la documentation).
Geany gère plus de 50 langages, dont C/C++, Javascript, HTML/CSS, PHP, Ruby, Lua, Python, Rust, Clojure, Lisb, assembleur, GLSL, cmake, Meson, Verilog, VHDL, abc, LaTex, etc.

