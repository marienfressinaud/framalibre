---
nom: "Adminer"
date_creation: "Vendredi, 13 janvier, 2017 - 22:28"
date_modification: "Lundi, 10 mai, 2021 - 13:54"
logo:
    src: "images/logo/Adminer.png"
site_web: "https://www.adminer.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "ADMINER, un produit surprenant de simplicité et parfaitement adapté à un usage épisodique des bases de données"
createurices: "Jakub Vrána"
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "gestionnaire de base de données"
    - "base de données"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Adminer"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/adminer"
---

Adminer est une interface utilisateur libre qui masque la complexité de gestion des bases de données. L'application est fonctionnellement équivalente à phpMyAdmin. Elle est développée en langage PHP au sein d'un fichier unique. L'interface permet très facilement de créer, modifier, consulter ou supprimer les bases de données MYSQL/MARIADB,PostgreSQL, SQLite, MS-SQL, ORACLE, SimpleDB, Elasticsearch, MongoDB ainsi que leurs composants (tables de données, index, champs de données, ...).
Adminer étant un simple fichier PHP, il est compatible avec toutes les plateformes disposant d'un moteur php5 ou supérieur. L'installation est on ne peut plus simple ; il suffit de déposer le fichier php dans l'espace web d'un serveur HTTP et de faire pointer le navigateur sur le fichier adminer.php. Il est possible d'agrémenter l'interface par un des nombreux fichiers .css disponibles sur le site de l'éditeur.
Adminer gère une seule base de données qu'elle soit locale ou distante.

