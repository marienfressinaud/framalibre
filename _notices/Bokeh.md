---
nom: "Bokeh"
date_creation: "Mercredi, 22 mars, 2017 - 14:07"
date_modification: "Mercredi, 22 mars, 2017 - 14:39"
logo:
    src: "images/logo/Bokeh.png"
site_web: "http://www.bokeh-library-portal.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Un portail documentaire libre pour votre bibliothèque."
createurices: "Agence Française Informatique"
alternative_a: "Syracuse, Orphée, Décalog, Flora, BCDI, Co-libris"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métiers"
    - "bibliothèque"
    - "centre de documentation"
    - "opac"
    - "catalogue"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/bokeh"
---

Bokeh est tout à la fois :
Un OPAC pour permettre aux usagers de :
- Chercher dans vos documents et de visualiser les notices,
- Voir et gérer leurs emprunts, faire des réservations,
- Suggérer de nouvelles acquisitions,
- Constituer des paniers,
- Etc.
Un CMS permettant de créer un ou plusieurs portails ou mini-sites, selon les besoins (bibliothèque avec des annexes par exemple), pour proposer des sélections, des actualités...
Un agrégateur de contenu, pour enrichir vos notices (couvertures des documents, biographies des auteurs issues de Wikipédia, interviews récupérées sur le site de l'INA, etc.)
Une bibliothèque numérique pour proposer :
- Des e-books,
- De la vidéo à la demande,
- Ressources numérisés,
- Etc.

