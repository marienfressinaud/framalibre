---
nom: "ProfGramme"
date_creation: "Dimanche, 16 août, 2020 - 18:32"
date_modification: "Jeudi, 20 août, 2020 - 05:18"
logo:
    src: "images/logo/ProfGramme.png"
site_web: "https://github.com/DegrangeM/ProfGramme"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "ProfGramme est un logiciel permettant de créer facilement sa progression depuis une liste d'éléments."
createurices: "Mathieu Degrange"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "éducation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/profgramme"
---

ProfGramme est un logiciel permettant de créer facilement sa progression depuis une liste d'éléments (par exemple tirée d'un programme officiel). Ce logiciel est une simple page web qui peut être téléchargé sur son ordinateur pour être exécuté hors ligne.

