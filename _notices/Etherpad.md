---
nom: "Etherpad"
date_creation: "Samedi, 27 octobre, 2018 - 17:55"
date_modification: "Dimanche, 6 juin, 2021 - 19:38"
logo:
    src: "images/logo/Etherpad.png"
site_web: "http://etherpad.org/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Un éditeur de texte collaboratif et en temps réel !"
createurices: ""
alternative_a: "G Suite, Google Docs, Office 365, Microsoft Word"
licences:
    - "Licence Apache (Apache)"
tags:
    - "traitement de texte"
    - "rédaction"
    - "travail collaboratif"
    - "édition collaborative"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Etherpad"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/etherpad"
---

Etherpad est un service web de traitement de texte collaboratif et en temps réel. Les documents créés sont nommés pads et possèdent chacun une URL propre.
Toutes les fonctionnalités de base sont à disposition :
mise en forme,
gestion des titres,
listes,
identification des auteur·es,
sauvegarde des versions successives,
import / export (formats HTML,txt).
Un chat interne au pad facilite la discussion.
Le logiciel Etherpad est décentralisé : différentes instances, hébergées par des organismes divers (fournisseurs d'accès, membres du collectif CHATONS, etc.), permettent de l'utiliser. Cela permet d'éviter de concentrer toutes les données entre les mains d'un même acteur. Les instances peuvent être personnalisées (limitation de la durée d'hébergement du pad, modification du design, etc.).

