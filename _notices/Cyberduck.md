---
nom: "Cyberduck"
date_creation: "Vendredi, 5 août, 2022 - 11:37"
date_modification: "Vendredi, 5 août, 2022 - 11:37"
logo:
    src: "images/logo/Cyberduck.png"
site_web: "https://cyberduck.io"
plateformes:
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Cyberduck est un client open source FTP."
createurices: "David Kocher, Yves Langisch"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "ftp"
    - "client ftp"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Cyberduck"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cyberduck"
---

Cyberduck est un logiciel libre et open source de navigation et de stockage cloud qui supporte les protocoles FTP, SFTP, WebDAV, Amazon S3, OpenStack Swift, Backblaze B2, Microsoft Azure & OneDrive, Google Drive et Dropbox.

