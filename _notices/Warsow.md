---
nom: "Warsow"
date_creation: "Dimanche, 15 janvier, 2017 - 14:25"
date_modification: "Vendredi, 7 mai, 2021 - 11:06"
logo:
    src: "images/logo/Warsow.png"
site_web: "https://www.warsow.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Warsow est un FPS avec des graphismes cartoon et de nombreux mouvements disponibles."
createurices: "Fabrice « SoLomonK » Demurger"
alternative_a: "Quake, Quake 3 Arena"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "fps"
    - "multi-joueur"
    - "tir"
    - "action"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Warsow"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/warsow"
---

Warsow est un jeu de tir en vue subjective (first-person shooter : FPS) avec des graphismes cartoon.
Les mouvements y ont une place importante. Il en existe beaucoup, ils sont basés sur les sauts, les dash, l'environnement ou encore les armes.
Le jeu intègre un tutoriel pour appréhender ces mouvements.
Les modes disponibles sont :
Duel,
Duel Arena,
DM – Deathmatch,
FFA – Free for all,
Team-based gametypes,
TDM – Team Deathmatch,
CA – Clan Arena,
Bomb & Defuse,
CTF – Capture The Flag,
Race.
En plus de ces modes officiels, il existe des modes issues de la communauté.

