---
nom: "CalcPvAutonome"
date_creation: "Mardi, 4 avril, 2017 - 23:32"
date_modification: "Mardi, 4 avril, 2017 - 23:32"
logo:
    src: "images/logo/CalcPvAutonome.png"
site_web: "http://calcpvautonome.zici.fr/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
    - "le web"
langues:
    - "Français"
description_courte: "Outil de dimensionnement pour une installation photovoltaïque en site isolé (autonome)"
createurices: "David Mercereau"
alternative_a: ""
licences:
    - "Licence Beerware"
tags:
    - "calculatrice"
    - "simulation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/calcpvautonome"
---

CalcPvAutonome est un outil de dimensionnement pour une installation photovoltaïque en site isolé (autonome). Il vous aide à déterminer combien de batteries, de panneaux... sera nécessaire à votre autonomie électrique.

