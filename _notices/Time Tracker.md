---
nom: "Time Tracker"
date_creation: "Lundi, 27 avril, 2020 - 16:07"
date_modification: "Lundi, 27 avril, 2020 - 16:09"
logo:
    src: "images/logo/Time Tracker.png"
site_web: "https://www.mentdb.org/time_tracker.html"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Un time tracker simple et efficace"
createurices: "Jimmitry Payet"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "tracker"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/time-tracker"
---

Grâce à ce logiciel vous pourrez traquer le temps passé sur les projets de vos clients rapidement.
Vous créez des clients, et ajouter du temps passé avec un commentaire.
Le système s'occupe de compter les heures et de les résumer en fin de mois,  à vos clients.

