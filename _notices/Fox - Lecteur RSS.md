---
nom: "Fox - Lecteur RSS"
date_creation: "Jeudi, 17 mars, 2022 - 20:32"
date_modification: "Jeudi, 17 mars, 2022 - 20:32"
logo:
    src: "images/logo/Fox - Lecteur RSS.png"
site_web: "https://pigeoff.pw/fox/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
description_courte: "Lecteur libre de podcasts et de flux RSS pour Android"
createurices: "César Pichon"
alternative_a: "Feedly"
licences:
    - "Licence Apache (Apache)"
tags:
    - "internet"
    - "lecteur de flux rss"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/242669/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fox-lecteur-rss"
---

Comme sur Tinder mais avec vos propres flux RSS, faites votre sélection de podcasts et d'articles à lire plus tard. Glissez à gauche les articles qui ne vous intéressent pas, glissez à droite ceux que vous souhaitez garder. Vous aurez ensuite tout le temps qu'il vous faut pour les lire ou les écouter !
Fox RSS est un lecteur RSS simple et sympa à utiliser. Gratuit, libre et sans publicité.
Le code source est disponible sur Github.

