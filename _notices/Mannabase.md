---
nom: "Mannabase"
date_creation: "Vendredi, 21 septembre, 2018 - 16:00"
date_modification: "Samedi, 22 septembre, 2018 - 15:35"
logo:
    src: "images/logo/Mannabase.JPG"
site_web: "http://www.mannabase.com"
plateformes:
    - "Autre"
langues:
    - "English"
description_courte: "La Fondation People's currency vous verse  un revenu de base."
createurices: "Eric Stetson"
alternative_a: ""
licences:
    - "Domaine public"
tags:
    - "métiers"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mannabase"
---

La technologie utilisée pour produire les Manna est très différente de celle du Bitcoin puisque les Manna ont été préminés et qu'ils sont distribués tous les mois aux personnes qui en font la demande.
Ils ne consomment donc pas les quantités astronomique d'énergie comme le Bitcoin.
Le Manna est géré par une fondation basée aux Etats-Unis, même si sont nom de domaine se termine en .com
Alors que le Bitcoin ne profite qu'aux spéculateurs et aux investisseurs propriétaires de fermes à Bitcoin, le Manna est distribué de manière équitable.
Le Manna est écheangeable sur les plateformes de marché habituelles (telles que https://www.southxchange.com/Market/Book/MANNA/BTC).
et son prix(taux de change) va évoluer en fonction de l'utilisation qu'en feront ses utilisateurs, comme toutes les monnaies.

