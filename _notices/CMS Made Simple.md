---
nom: "CMS Made Simple"
date_creation: "Lundi, 2 janvier, 2017 - 17:04"
date_modification: "Lundi, 10 mai, 2021 - 13:27"
logo:
    src: "images/logo/CMS Made Simple.png"
site_web: "http://www.cmsmadesimple.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "CMS Made Simple est Puissant pour les professionnels, Simple pour les utilisateurs."
createurices: "Ted Kulp, and The CMS Made Simple Development Team (The Dev Team)"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "php"
    - "mysql"
lien_wikipedia: "https://fr.wikipedia.org/wiki/CMS_Made_Simple"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cms-made-simple"
---

CMS Made Simple (CMSMS) est un système de gestion de contenu web permettant de construire très rapidement et simplement un site Web.
Il s’appuie sur PHP et MySQL et permet de produire du code conforme aux standards du Web.
CMSMS embarque avec lui de nombreux exemples qui permettent d'avoir une installation fonctionnelle, avec quelques idées de graphisme pré-installées.
Quelques caractéristiques :
une organisation qui sépare les pages créées par les utilisateurs, les gabarits et les feuilles de style,
chaque gabarit est lié à une ou plusieurs feuille(s) de styles (CSS) — il est possible de multiplier les gabarits selon le besoin de votre site,
les menus sont générés automatiquement en fonction de l'organisation des pages, il est possible de réordonner simplement les pages donc les menus,
la possibilité d'installer des fonctions supplémentaires directement depuis la partie administration du site,
Exemples : https://welovecmsms.com

