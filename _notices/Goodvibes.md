---
nom: "Goodvibes"
date_creation: "Lundi, 16 janvier, 2017 - 08:43"
date_modification: "Lundi, 14 mai, 2018 - 02:53"
logo:
    src: "images/logo/Goodvibes.png"
site_web: "https://gitlab.com/goodvibes/goodvibes"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Goodvibes est un lecteur de radios internet pour GNU/Linux."
createurices: "Arnaud Rebillout"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "lecteur audio"
    - "lecteur radio"
    - "audio"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/goodvibes"
---

Goodvibes est un lecteur de radios internet pour GNU/Linux. Il permet d'avoir ses radios préférées à portée de main.
L'interface graphique est légère, minimaliste. Le logiciel s'intègre au mieux dans les différents environnements de bureau existants.

