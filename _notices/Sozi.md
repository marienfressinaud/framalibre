---
nom: "Sozi"
date_creation: "Mardi, 27 décembre, 2016 - 13:21"
date_modification: "Dimanche, 3 mars, 2019 - 22:37"
logo:
    src: "images/logo/Sozi.png"
site_web: "http://sozi.baierouge.fr/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Créer des présentations dynamiques par effet de zoom à partir d'une image svg."
createurices: "Guillaume Savaton"
alternative_a: "Prezi"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "bureautique"
    - "présentation"
    - "diaporama"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/sozi"
---

A l'origine il s'agissait d'une extension pour Inkscape mais une version autonome a vu le jour.
Sozi permet de faire des présentations dynamiques en quelques minutes.
La présentation est un poster et les diapositives sont un ensemble de zooms, translations et rotations sur une partie de ce poster. Les transitions sont configurables et fluides.
Cadrez, prenez un instantané, choisissez la transition puis passez à la diapositive suivante.
Le fichier de sortie est en html et peut être exécuté dans n'importe quel navigateur de façon autonome.
La combinaison de Inkscape, LibreOffice Draw ou d'un logiciel de cartes heuristiques et de sozi permet de gagner un  temps considérable dans l'élaboration de vos présentations.

