---
nom: "Tupi"
date_creation: "Samedi, 14 janvier, 2017 - 00:25"
date_modification: "Samedi, 29 avril, 2017 - 11:36"
logo:
    src: "images/logo/Tupi.png"
site_web: "http://www.maefloresta.com/tupi/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Le dessin animé à portée de clic"
createurices: "Gustav González, Ktoon"
alternative_a: "MonkeyJam, Adobe Animate CC, Prezi, stopmotion, Flash"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "animation"
    - "2d"
    - "dessin vectoriel"
    - "stopmotion"
    - "dessin animé"
    - "tablette graphique"
    - "webcam"
    - "éditeur vidéo"
lien_wikipedia: "https://en.wikipedia.org/wiki/Tupi_(software)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tupi"
---

L'application de référence pour créer des animations simplement et efficacement quelque soit votre niveau d'infographiste (de 6 à 106 ans et plus)
Tupi est un outil génial et ultra simple à prendre en main pour créer des animations.
Il intègre des outils pour dessiner au sein de l'application, pour automatiser certains mouvements et déplacement, gérer le comportement d'éléments de décor, faire de l'animation image par image à partir de vos photos ou en utilisant directement votre webcam.
Basé sur l'utilisation d'image au format SVG (mais accepte aussi les formats jpg, png, …), il optimise l'élégance des lignes et permet d'ajuster les courbes notamment grâce à une visualisation pelure d'oignon (aperçu intégré de l'image d'avant et image d'après).
Tupi tire son nom d'une tribu amazonienne rappelant ses origines amazoniennes.
Valorise et justifie rapidement l'utilisation d'une tablette graphique.
Un mot ? Génial !

