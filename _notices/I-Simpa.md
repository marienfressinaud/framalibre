---
nom: "I-Simpa"
date_creation: "Lundi, 9 juillet, 2018 - 20:27"
date_modification: "Mercredi, 12 mai, 2021 - 15:12"
logo:
    src: "images/logo/I-Simpa.jpg"
site_web: "http://i-simpa.ifsttar.fr/"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Interface logicielle permettant de modéliser la propagation d'ondes acoustiques dans des géométries complexes."
createurices: "Nicolas Fortin, Judicaël Picaut"
alternative_a: "Catt-Acoustic, Odeon, Ease"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "physique"
    - "simulation"
    - "acoustique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/i-simpa"
---

I-Simpa est une interface logicielle de modélisation acoustique. Plus précisément, I-Simpa permet de modéliser la propagation tridimensionnelle des ondes acoustiques dans des géométries complexes. Cet environnement de travail est fourni avec deux codes de calcul :
SPPS (Simulation de la propagation de particules sonores) est un code basé sur la méthode du tir de particules,

TCR (Théorie classique de la réverbération) permet l'application numérique de la théorie de Sabine basée sur l'hypothèse de champs diffus,
Un troisième code, MD, est en cours de développement.
Différents nombreuses fonctionnalités de pré et post-traitement sont intégrées :
import de géométries,
import de propriétés de matériaux,
calcul de réponse impulsionnelles,
calcul des paramètres acoustiques (norme ISO 3382),
cartographies 2D,
animations,
intégration de scripts Python.

