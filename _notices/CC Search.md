---
nom: "CC Search"
date_creation: "Mercredi, 28 avril, 2021 - 15:08"
date_modification: "Samedi, 7 août, 2021 - 14:11"
logo:
    src: "images/logo/CC Search.png"
site_web: "https://search.creativecommons.org/"
plateformes: ""
langues: "English"
description_courte: "Un méta-moteur pour rechercher facilement des ressources multimédia sous licence libre sur le Web"
createurices: ""
licences: "Multiples licences"
tags:
    - "Multimédia"
    - "image"
    - "vidéo"
    - "audio"
lien_wikipedia: ""
lien_exodus: ""
alternative_a: ""
mis_en_avant: "oui"
---

CC Search est un méta-moteur de recherche d'images, d'audios et de vidéos libres de droits. Il est possible de filtrer les résultats selon la licence des images (tous les licences Creative Commons) et leur source. Il agrège les ressources des sites comme Flickr, Wikimedia Commons ou Youtube.

