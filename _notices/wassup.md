---
nom: "wassup"
date_creation: "Vendredi, 14 février, 2020 - 12:51"
date_modification: "Vendredi, 14 février, 2020 - 12:52"
logo:
    src: "images/logo/wassup.png"
site_web: "https://git.42l.fr/neil/wassup"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Outil pour envoyer facilement des newsletters / lettres d'information en ligne de commande."
createurices: "neil"
alternative_a: "mailchimp"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "bureautique"
    - "cli"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/wassup"
---

Cet outil permet d'envoyer des mails en masse. Simple à configurer au travers d'un fichier de configuration, il supporte un template (modèle) de mail par défaut et envoie des mails en texte brut et en HTML, en respectant les normes d'envoi pour éviter le rejet du mail.
Il vous est possible d'écrire votre newsletter en Markdown.
Il vous suffit de personnaliser les modèles d'email pour vos besoins, de spécifier votre liste de destinataires, de donner les identifiants d'un compte mail et votre newsletter est envoyée.
Écrit en Rust, le programme est facile à auditer : il tient sur 400 lignes de code.
Attention : son utilisation n'est PAS adaptée pour l'envoi d'emails à plus de 100 personnes.

