---
nom: "FreeFileSync"
date_creation: "Lundi, 24 avril, 2017 - 17:29"
date_modification: "Mercredi, 12 mai, 2021 - 14:53"
logo:
    src: "images/logo/FreeFileSync.png"
site_web: "https://www.freefilesync.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "FreeFileSync vous aide à synchroniser fichiers et dossiers entre deux support grâce à une interface intuitive."
createurices: "zenju@freefilesync.org"
alternative_a: "SyncToy"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "synchronisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FreeFileSync"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freefilesync"
---

FreeFileSync est un logiciel gratuit et open source qui vous aide à synchroniser fichiers et dossier pour Windows, Linux, et macOs. Il est conçu pour vous faire gagner du temps lors de la préparation et de l'exécution d'une sauvegarde tout en ayant une jolie interface.

