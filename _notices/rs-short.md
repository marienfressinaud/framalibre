---
nom: "rs-short"
date_creation: "Vendredi, 14 février, 2020 - 12:31"
date_modification: "Vendredi, 7 mai, 2021 - 11:19"
logo:
    src: "images/logo/rs-short.png"
site_web: "https://git.42l.fr/42l/rs-short"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Raccourcisseur de liens minimaliste en Rust."
createurices: "neil"
alternative_a: "bit.ly, goo.gl"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "url"
    - "anonymat"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/rs-short"
---

Raccourcisseur de liens qui consomme très peu de mémoire RAM (~10 MB), qui se veut très minimaliste et simple d'utilisation.
Il est possible de définir une adresse personnalisée pour la redirection du lien, de compter les clics sur chaque lien et de supprimer un lien sans laisser aucune trace.
Dispose d'un CAPTCHA (sans Google reCAPTCHA) afin d'éviter le spam.
Fonctionne entièrement sans Javascript ni framework CSS (le logiciel s'adapte facilement à n'importe quelle charte graphique existante).
Seulement 1000 lignes de code.

