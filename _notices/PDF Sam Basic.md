---
nom: "PDF Sam Basic"
date_creation: "Mardi, 23 mai, 2017 - 18:37"
date_modification: "Mardi, 9 janvier, 2018 - 10:11"
logo:
    src: "images/logo/PDF Sam Basic.png"
site_web: "https://pdfsam.org/pdfsam-basic/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Manipuler des fichiers PDF : découper, extraire, fusionner, etc"
createurices: "Andrea Vacondio"
alternative_a: "PDF-XChange, ABBY PDF, Foxit Reader, Nitro Reader, Adobe Acrobat Pro"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "bureautique"
    - "pdf"
    - "manipulation de pdf"
lien_wikipedia: "https://en.wikipedia.org/wiki/PDF_Split_and_Merge"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pdf-sam-basic"
---

PDFsam Basic est un utilitaire bureautique pour découper et/ou mêler, diviser, fusionner, extraire(extrait) des pages, faire tourner et mélanger des documents PDF.
Il est multiplateforme, libre et gratuit en mode Basic.

