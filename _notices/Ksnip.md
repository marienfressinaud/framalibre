---
nom: "Ksnip"
date_creation: "Mercredi, 24 juin, 2020 - 23:46"
date_modification: "Mercredi, 12 mai, 2021 - 16:30"
logo:
    src: "images/logo/Ksnip.png"
site_web: "http://ksnip.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Lâchez-vous sur la capture d'écran. Rapide léger, ultra-paramétrable, l'amie de vos démos et de vos tutoriels."
createurices: "Damir Porobic"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "capture d'écran"
    - "infographie"
    - "didactique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/ksnip"
---

Ksnip est une application de capture d'écran aussi simple qu'efficace.
* Capture écran, fenêtre,zone …
* Avec ou sans curseur
* Éditeur intégré avec outil de floutage, de numérotation, de fléchage, zonage
* Possibilité d'éditer et d'annoter n'importe quelle image (pas seulement les captures)
* Capture en rafale
* Envoi sur internet (sites dédiés, ftp, cloud …)
* Raccourcis clavier
* Multi-environnement
Si vous connaissiez Shutter, vous pourriez bien l'oublier au profit de Ksnip

