---
nom: "GEM"
date_creation: "Samedi, 27 janvier, 2018 - 10:50"
date_modification: "Samedi, 6 avril, 2019 - 11:05"
logo:
    src: "images/logo/GEM.png"
site_web: "https://gem.tuxfamily.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "GEM est un logiciel de gestion de collection de jeux rétro et des émulateurs associés."
createurices: "PacMiam"
alternative_a: ""
licences:
    - "Domaine public"
    - "Licence Art Libre (LAL)"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "émulateur"
    - "collection"
    - "console"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gem"
---

GEM est un logiciel qui permet à l’utilisateur de gérer une collection de jeux-vidéo anciens au sein d’une interface simple et fonctionnelle.
Vous pourrez ainsi, retrouver les fonctionnalités suivantes :
Configuration des consoles et émulateurs depuis l’interface graphique
Possibilité de spécifier un émulateur spécifique par jeu
Affichage des captures d’écran dans un visualiseur natif
Possibilité d’annoter le jeu pendant que celui-ci est en lancement
Filtrage des listes pour faciliter la recherche d’un jeu en particulier
Affichage des jeux en liste détaillée ou en grille
Et plein d’autres fonctionnalités !

