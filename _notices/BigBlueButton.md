---
nom: "BigBlueButton"
date_creation: "Jeudi, 7 mai, 2020 - 09:16"
date_modification: "Mercredi, 12 mai, 2021 - 14:45"
logo:
    src: "images/logo/BigBlueButton.png"
site_web: "https://bigbluebutton.org/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Logiciel de vidéo-conférence spécialisé pour l'apprentissage en ligne"
createurices: ""
alternative_a: "Skype, Google Hangouts, Zoom"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "visioconférence"
    - "appels audio"
    - "apprentissage"
    - "école"
lien_wikipedia: "https://en.wikipedia.org/wiki/BigBlueButton"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/bigbluebutton"
---

BigBlueButton comporte les fonctionnalités attendues d'un logiciel de visioconférence (plusieurs vidéos, chat, partage d'écran etc), mais aussi et surtout des outils pour faciliter l'enseignement à distance: tableau interactif, partage de diapositives, enregistrement et re-lecture de sa présentation, sondages, riche set d'émojis, etc.
Un logiciel très complet à découvrir.

