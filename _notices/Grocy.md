---
nom: "Grocy"
date_creation: "Dimanche, 10 mai, 2020 - 12:42"
date_modification: "Dimanche, 10 mai, 2020 - 12:42"
logo:
    src: "images/logo/Grocy.png"
site_web: "https://grocy.info/"
plateformes:
    - "GNU/Linux"
    - "Autre"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Grocy, l'ERP au-delà du frigo."
createurices: "Bernd Bestel"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "erp"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/grocy"
---

Grocy est une solution web auto-hébergée de gestion des stocks et du ménage pour la maison.
Ça permet de gérer les stocks, les consommations, les recettes, les listes de courses en fonction des stocks et des recettes prévues,...
Le tout est géré via une interface web compatible avec tous les formats (du smartphone à l'écran de pc), et peut intégrer un lecteur de code barres pour faciliter les encodages.

