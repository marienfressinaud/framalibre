---
nom: "ParaView"
date_creation: "Lundi, 9 juillet, 2018 - 21:42"
date_modification: "Mercredi, 12 mai, 2021 - 15:12"
logo:
    src: "images/logo/ParaView.png"
site_web: "https://www.paraview.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "ParaView est un logiciel scientifique de visualisation de données."
createurices: ""
alternative_a: ""
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "science"
    - "physique"
    - "visualisation de données"
    - "calcul scientifique"
    - "post processing"
lien_wikipedia: "https://fr.wikipedia.org/wiki/ParaView"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/paraview"
---

ParaView est un logiciel de visualisation de données utilisable dans de nombreux domaines scientifiques : mécanique, génie civil, mécanique des fluides, astrophysique. Il permet ainsi de visualiser des maillages et des résultats de calculs aux éléments finis. ParaView fait partie de la plateforme logicielle Salome, qui intègre des outils de pré processing et de post processing.

