---
nom: "Scenari"
date_creation: "Mercredi, 19 avril, 2017 - 22:16"
date_modification: "Jeudi, 5 janvier, 2023 - 19:19"
logo:
    src: "images/logo/Scenari.png"
site_web: "https://scenari.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Une suite logicielle qui permet la création de documents multisupports (site web, présentation, diapo, pdf, …)"
createurices: ""
alternative_a: "Microsoft Office, Microsoft Word, Microsoft PowerPoint"
licences:
    - "Licence CECILL (Inria)"
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
tags:
    - "bureautique"
    - "chaîne éditoriale"
    - "création de site web"
    - "diaporama"
    - "traitement de texte"
    - "exerciseur"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Scenari"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/scenari"
---

Scenari est une suite logicielle qui permet de créer des sites web, des présentations, des diaporamas, des pdf, etc, à partir d'une seule source.
Les avantages :

gain de temps dans la production et la maintenance de ses documents ;
travail à plusieurs via le réseau ou le cloud ;
mise en forme est gérée par Scenari ce qui permet de se concentrer sur son rôle d'auteur.

Il existe plusieurs modèles documentaires :

Projets, présentations : OptimOffice
Enseignement parcours linéaire: Opale, Technopale, Canoprof
Étude de cas, jeu sérieux: Topaze
Documentation logicielle : Dokiel
Enrichissement audio/vidéo : WebMedia
les autres modèles : IdKey, Lexique, Rubis, Emeraude…


