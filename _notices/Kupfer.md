---
nom: "Kupfer"
date_creation: "Samedi, 31 décembre, 2016 - 01:32"
date_modification: "Mercredi, 12 mai, 2021 - 16:35"
logo:
    src: "images/logo/Kupfer.png"
site_web: "https://kupferlauncher.github.io/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Kupfer est un lanceur d'applications écrit en Python."
createurices: "Ulrik Sverdrup"
alternative_a: "LaunchBar, Alfred"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "lanceur d'applications"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/kupfer"
---

Kupfer est une interface qui permet lancer rapidement une application ou de trouver des documents. Lancez-la via un raccourci clavier, ensuite tapez quelques lettres pour trouver rapidement l'application ou le document que vous cherchez. De nombreux plugins sont disponibles afin d'étendre ses fonctionnalités.

