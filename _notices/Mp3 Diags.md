---
nom: "Mp3 Diags"
date_creation: "Samedi, 13 novembre, 2021 - 16:51"
date_modification: "Samedi, 13 novembre, 2021 - 17:07"
logo:
    src: "images/logo/Mp3 Diags.png"
site_web: "http://mp3diags.sourceforge.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "MP3 Diags est une application qui permet aux utilisateurs finaux d'identifier les problèmes avec leurs fichier"
createurices: "Ciobi"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "audio"
    - "enrichissement audio"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mp3-diags"
---

Contrairement à certains programmes conçus pour traiter un seul sujet (comme la correction des en-têtes VBR ou l'ajout d'une pochette), MP3 Diags est une solution unique qui identifie plus de 50 problèmes différents dans les fichiers MP3 et fournit les moyens d'en résoudre plusieurs ( eh bien, tout n'est pas réparable ; vous ne pouvez pas faire sonner un fichier à 64 kbit/s comme un fichier à 256 kbit/s.)
Certains des problèmes les plus importants rencontrés :
balises cassées / en-têtes / audio
balises / en-têtes en double
placement incorrect des balises / en-têtes (ID3V2, ID3V1, LAME, Xing, ...)
son de mauvaise qualité
en-tête VBR manquant
info piste manquante / pochette
info piste cassée / pochette
données de normalisation manquantes
problèmes de codage des caractères (pour les langues autres que l'anglais)

