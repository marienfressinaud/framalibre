---
nom: "microG Project"
date_creation: "Samedi, 25 avril, 2020 - 16:33"
date_modification: "Mercredi, 12 mai, 2021 - 14:44"
logo:
    src: "images/logo/microG Project.png"
site_web: "https://microg.org/"
plateformes:
    - "Android"
langues:
    - "English"
description_courte: "microG Project est un projet de réimplémentation libre des services privateurs de Google pour Android."
createurices: "Marvin Wißfeld"
alternative_a: "Services Google Play"
licences:
    - "Licence Apache (Apache)"
tags:
    - "système"
    - "android"
    - "utilitaire"
lien_wikipedia: "https://en.wikipedia.org/wiki/MicroG"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/microg-project"
---

microG Project est un projet de réimplémentation libre des services privateurs de Google pour Android. Le but de ce projet est de permettre l'accès aux APIs de Google, sur lesquelles s'appuient de nombreuses applications Android pour fonctionner, sans devoir utiliser la surcouche lourde et propriétaire des applications Google. La collecte de données par Google est ainsi fortement réduite.
microG s'adresse principalement aux utilisateurs de systèmes d'exploitation Android personnalisés, tels que LineageOS, qui souhaitent accéder à l'ensemble de l'écosystème Android, sans pour autant effectuer de compromis sur la protection de leurs données personnelles.

