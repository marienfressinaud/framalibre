---
nom: "PMB"
date_creation: "Mardi, 27 décembre, 2016 - 21:42"
date_modification: "Vendredi, 5 août, 2022 - 13:34"
logo:
    src: "images/logo/PMB.png"
site_web: "http://www.sigb.net"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "PMB est un logiciel libre de gestion de bibliothèques. Il comprend un module portail (OPAC)."
createurices: ""
alternative_a: "Syracuse, Orphée, Décalog, Flora, BCDI, Co-libris"
licences:
    - "Licence CECILL (Inria)"
tags:
    - "métiers"
    - "bibliothèque"
    - "ged"
    - "bibliographie"
    - "gestion documentaire"
    - "sigb"
    - "centre de documentation"
    - "catalogue"
    - "opac"
    - "livres"
lien_wikipedia: "https://fr.wikipedia.org/wiki/PMB_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/pmb"
---

PMB est un système intégré de gestion de bibliothèques. Il possède un module de gestion (prêts, catalogues, éditions...) et un portail type OPAC (Online Public Access Catalog).
il permet d'importer des notices de la BNF au format Z39.50; import/export également en unimarc et XML,
grilles de catalogage personnalisables,
suggestions et acquisitions, 
statistiques du fond,
OPAC 2.0, etc
Il supporte des fonds de 500 000 notices.

