---
nom: "PDF-Shuffler"
date_creation: "Samedi, 3 novembre, 2018 - 21:20"
date_modification: "Lundi, 5 novembre, 2018 - 11:13"
logo:
    src: "images/logo/PDF-Shuffler.png"
site_web: "https://sourceforge.net/projects/pdfshuffler/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Une application pour éditer des documents PDF."
createurices: "Konstantinos Poulios"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "pdf"
    - "manipulation de pdf"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pdf-shuffler"
---

PDF-Shuffler est une petite application python-gtk qui permet de fusionner ou de diviser des documents PDF et de pivoter, rogner et réorganiser leurs pages à l'aide d'une interface interactive et intuitive.
C'est une interface graphique au programme en ligne de commande python-pyPdf.

