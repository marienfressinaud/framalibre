---
nom: "CentOs"
date_creation: "Mardi, 3 janvier, 2017 - 01:09"
date_modification: "Vendredi, 4 janvier, 2019 - 16:51"
logo:
    src: "images/logo/CentOs.jpg"
site_web: "https://www.centos.org"
plateformes:
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Système d'exploitation Linux orienté serveur ou poste de travail, dérivé de la distribution \"Red-Hat\"."
createurices: ""
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "linux"
    - "système d'exploitation (os)"
    - "serveur"
lien_wikipedia: "https://fr.wikipedia.org/wiki/CentOS"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/centos"
---

CentOS est un système d'exploitation Linux orienté serveur ou poste de travail. Il est dérivé de la distribution "Red-Hat" de laquelle il reprend l'essentielle des fonctionnalités. La version majeure de CentOS 7 cohabite avec la version 6. La version 5 n'est plus soutenue depuis mars 2017.
Centos 7 est compatible avec les architectures 64 bits en mode multiprocesseurs. Une version 32 bits existe néanmoins même si tous les paquets logiciels n'ont pas été portés sur cette architecture. Une version ARM existe aussi pour le Raspberry2 et 3.
Le système d'exploitation est doté d'un environnement graphique de type Gnome ou KDE. L'installation est simplifiée, assistée et se déroule en mode pseudo-graphique.
Les distributions sont disponibles en téléchargement au format ISO sur le site de l'éditeur. On les trouve en version DVD, complète, minimale et parfois en netInstall (installation réseau).
De base, le noyau est en version 3.

