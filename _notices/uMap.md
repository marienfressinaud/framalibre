---
nom: "uMap"
date_creation: "Lundi, 29 octobre, 2018 - 12:27"
date_modification: "Mardi, 30 octobre, 2018 - 18:33"
logo:
    src: "images/logo/uMap.png"
site_web: "https://github.com/umap-project/umap/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Un outil en ligne pour personnaliser vos cartes."
createurices: ""
alternative_a: "Google Maps"
licences:
    - "Licence publique f***-en ce que vous voulez (WTFPL)"
tags:
    - "carte géographique"
    - "cartographie"
    - "édition de cartes géographiques"
    - "osm"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/umap"
---

uMap est un outil en ligne d'édition et de partage de cartes. Il permet de créer des itinéraires, de marquer des points d'intérets, de délimiter des zones, de choisir le fond de carte le plus approprié, etc.
uMap est un logiciel décentralisé : le service est proposé par plusieurs organisations, comme Framasoft ou OpenStreetMap France. C'est à partir de ces sites que vous pourrez utiliser uMap.
Une série de tutoriels est disponible pour découvrir l'étendue des fonctionnalités de uMap.
Les données constituant les fonds de carte proviennent du projet OpenStreetMap.

