---
nom: "Algem"
date_creation: "Vendredi, 9 juin, 2017 - 08:52"
date_modification: "Mercredi, 6 septembre, 2023 - 13:18"
logo:
    src: "images/logo/Algem.png"
site_web: "https://www.algem.net"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "Algem est un logiciel libre de gestion d’activités socio-culturelles."
createurices: "Jean-Marc Gobat, Bruno Mauguil"
alternative_a: "Rhapsodie, iMuse, Musicole"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "éducation"
    - "espace numérique de travail (ent)"
    - "gestion"
    - "gestionnaire de contenus"
    - "facturation"
    - "planning"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/algem"
---

Ce logiciel de gestion, initialement conçu pour les écoles de musiques actuelles, répond aussi aux besoins des associations, des salles de répétition, des M.J.C ou des centres de formation.
Publié sous licence AGPL, il présente un grand nombre de fonctionnalités :

gestion de contacts et d’adhérents
gestion de groupes
planification de cours et de répétition
réservation de salles et gestion de matériel
gestion comptable,facturation
feuilles de présence et heures enseignants
exports csv, mailing,statistiques

Un ENT (Environnement de Travail Numérique), permet les réservations à distance, la gestion du suivi pédagogique ou la consultation des plannings hebdomadaires de chaque membre.
En service dans de nombreuses structures, parmi lesquelles "Jazz à Tours" ou "Music'Halle" (pour ne citer que les plus importantes), Algem est configurable et extensible à volonté.

