---
nom: "Ferment"
date_creation: "Lundi, 15 janvier, 2018 - 05:03"
date_modification: "Mardi, 6 décembre, 2022 - 07:54"
logo:
    src: "images/logo/Ferment.png"
site_web: "https://github.com/fermentation/ferment"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "English"
description_courte: "Une application Peer to Peer de publication et de streaming audio."
createurices: ""
alternative_a: "Spotify"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "musique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ferment"
---

Ferment se positionne comme une alternative à Spotify utilisant la technologie P2P (Peer to Peer). L'application utilise donc un fonctionnement totalement décentralisé.
L'application utilise les logiciels SSB, WebTorrent et Electron.
En mai 2018, le développement semble arrêté et le logiciel n'est pas disponible.

