---
nom: "CryptPad"
date_creation: "Samedi, 3 novembre, 2018 - 15:34"
date_modification: "mardi, 26 décembre, 2023 - 11:54"
logo:
    src: "images/logo/CryptPad.png"
site_web: "https://cryptpad.org"
plateformes:
    - "le web"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Avec CryptPad, vous pouvez créer des documents collaboratifs rapidement pour prendre des notes à plusieurs."
createurices: "XWiki SAS"
alternative_a: "Google Docs, Office 365"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "traitement de texte"
    - "rédaction"
    - "travail collaboratif"
    - "édition collaborative"
    - "présentation"
    - "kanban"
    - "sondage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/cryptpad"
---

Avec CryptPad, vous pouvez créer des documents collaboratifs rapidement pour prendre des notes à plusieurs. CryptPad est une alternative respectant la vie privée aux outils office et aux services cloud populaires. Tout le contenu stocké dans CryptPad est chiffré avant d'être envoyé, ce qui signifie que personne ne peut accéder à vos données à moins que vous ne leur donniez les clés (même pas nous).
À l'instar du logiciel Etherpad, Cryptpad est décentralisé : différentes instances, hébergées par des organismes divers (fournisseurs d'accès, membres du collectif CHATONS, etc.), permettent de l'utiliser. Cela permet d'éviter de concentrer toutes les données entre les mains d'un même acteur. Les instances peuvent être personnalisées (limitation de la durée d'hébergement du pad, modification du design, etc.).
L'instance officielle à l'adresse https://cryptpad.fr est administrée par XWiki SAS, l'entreprise française qui a créé et qui maintient CryptPad.


