---
nom: "Gajim"
date_creation: "Dimanche, 25 février, 2018 - 20:34"
date_modification: "Mercredi, 12 mai, 2021 - 16:15"
logo:
    src: "images/logo/Gajim.png"
site_web: "https://gajim.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Gajim est un logiciel libre client de messagerie instantanée pour le réseau standard ouvert Jabber (XMPP)."
createurices: ""
alternative_a: "Facebook Messenger, Google Hangouts"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "jabber"
    - "xmpp"
    - "messagerie instantanée"
    - "voip"
    - "visioconférence"
    - "communication chiffrée"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Gajim"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/gajim"
---

Gajim est un client Jabber libre écrit en python qui utilise la bibliothèque graphique GTK. Il est disponible pour les systèmes GNU/Linux, FreeBSD et Windows. Gajim ne fait que du XMPP, mais il le fait bien, contrairement à beaucoup d’autres clients implémentant XMPP parmi d’autres protocoles pas toujours correctement.
C’est un client écrit en Python, dont l’interface graphique s’appuie sur GTK+, et qui fonctionne donc sur toutes les plateformes, même s’il propose plus de fonctionnalités sous Linux. Parmi les plus intéressantes on trouve :
- Support de TLS, SSL, GPG, et chiffrage point à point
- Support de l’enregistrement des transports
- Conversations audio / vidéo
- Transfert de fichiers
- Système de plugins…

