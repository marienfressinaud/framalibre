---
nom: "Emendare"
date_creation: "Vendredi, 1 février, 2019 - 12:00"
date_modification: "Vendredi, 1 février, 2019 - 12:16"
logo:
    src: "images/logo/Emendare.png"
site_web: "https://emendare.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
langues:
    - "Français"
description_courte: "Sécuriser la mise en place démocratique de textes ou lois."
createurices: "Jimmy Leray"
alternative_a: "Cap Collectif"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "vote"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/emendare"
---

Suite à Nuit Debout, des programmeurs ont décidé de porter un projet de discussion, puis de vote de textes, puis d'archivage.
Il est prévu de pouvoir vérifier humainement les votes, seul moyen d'empêcher la modification des votes.
Aussi un chat anonyme est prévu pour discuter des textes afin de mieux les défendre.

