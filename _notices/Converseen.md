---
nom: "Converseen"
date_creation: "Lundi, 6 mai, 2019 - 13:07"
date_modification: "Lundi, 10 mai, 2021 - 13:35"
logo:
    src: "images/logo/Converseen.png"
site_web: "http://converseen.fasterland.net/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Convertisseur d'images de plus de 100 formats d'images !"
createurices: "Francesco Mondello"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "convertisseur"
    - "image"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/converseen"
---

Grâce à ImageMagick, la puissante bibliothèque de manipulation d’images sur laquelle repose le programme peut prendre en charge plus de 100 formats d’image, notamment DPX, EXR, GIF, JPEG, JPEG-2000, PhotoCD, PNG, Postscript, SVG, TIFF, etc. autres.

