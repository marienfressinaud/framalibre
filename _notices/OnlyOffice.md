---
nom: "OnlyOffice"
date_creation: "Mercredi, 27 juin, 2018 - 16:58"
date_modification: "Mercredi, 12 mai, 2021 - 15:32"
logo:
    src: "images/logo/OnlyOffice.jpg"
site_web: "https://www.onlyoffice.com/fr/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "Autres langues"
description_courte: "Une suite Office en ligne, libre et hébergée sur votre serveur ?
Découvrez OnlyOffice !"
createurices: "Ascensio System"
alternative_a: "Microsoft Office, Google Docs, Microsoft Word, Microsoft Excel, Microsoft PowerPoint, Google Drive"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "bureautique"
    - "traitement de texte"
    - "présentation"
    - "diaporama"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Onlyoffice"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/onlyoffice"
---

Réalisé en ASP.NET et disponible dans de multiples langues (incluant le français), cette suite Office complète se distingue en différents modules.
L’éditeur de documents, basé sur Open XML et pleinement compatible avec Word, Excel et PowerPoint disponible à travers un simple navigateur, avec édition collaborative en temps réel et connectable avec OwnCloud, NextCloud...
La plateforme de collaboration, ajoutant à l'éditeur de documents des fonctionnalités de communications (xmpp, courriel, wiki), d'organisation (CRM, calendrier, gestion de projets ...), en fédérant les différents utilisateurs en interne.
Les versions Desktop et Mobile (Android et iOS) permettant l'édition des documents pouvant se substituer à LibreOffice dans sa version la plus basique (Writer, Calc et Impress).
Une version gratuite pour un usage individuel ONLYOFFICE Personal est mise à disposition sur leurs serveurs, vous permettant de vous faire votre propre avis.

