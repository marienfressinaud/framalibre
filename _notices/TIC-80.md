---
nom: "TIC-80"
date_creation: "Mardi, 17 octobre, 2023 - 22:13"
date_modification: "Vendredi, 27 octobre, 2023 - 10:57"
logo:
    src: "images/logo/TIC-80.png"
site_web: "https://tic80.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Autre"
langues:
    - "English"
description_courte: "Un fantasy computer pour créer, jouer et partager de petits jeux."
createurices: "Vadim Grigoruk"
alternative_a: "PICO-8"
licences:
    - "Licence MIT/X11"
tags:
    - "jeu"
    - "ide"
    - "moteur de jeu"
    - "créativité"
    - "développement de jeu vidéo"
    - "jeu vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/TIC-80"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tic-80"
---

Fantasy computer (ou console), c'est à dire Environnement à la fois de jeu et de développement intégré.
Sa facilité de programmation avec notamment Lua et son environnement intégré en font un bon outil de prototypage de jeu.

Supporte les langages de programmation Lua (principal), Fennel, JavaScript, MoonScript, Wren, Squirrel et d'autres
Export HTML5 (WASM), .tic (propre format), .PNG (intégrant le jeu chargeable par TIC-80), et dans la version pro, binaires pour Linux, Windows, Nintendo 3DS, Rasberry Pi Bare-metal
Éditeur de son FM, et un éditeur de musique de type tracker
Éditeur de sprites et de terrain de jeu
Il est utilisé dans la scène démo, notamment pour du tinycode (démos de moins de 512 caractères).


