---
nom: "Cagette"
date_creation: "Samedi, 17 juin, 2017 - 01:08"
date_modification: "Mardi, 11 mai, 2021 - 23:14"
logo:
    src: "images/logo/Cagette.jpg"
site_web: "http://www.cagette.net/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Français"
description_courte: "Le logiciel libre du circuit court"
createurices: "AMAP de Bordeaux Nansouty"
alternative_a: "La ruche qui dit oui"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "amap"
    - "circuit court"
    - "économie"
    - "agriculture"
    - "gestion"
    - "gestion de la relation client"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/cagette"
---

Conçu pour les AMAPs et les groupements d'achat, Cagette est un logiciel libre qui gère les commandes groupées, le planning de livraison, la gestion des adhérents et bien sûr les produits.
Il est bien sûr possible de l'installer sur votre propre serveur, mais maintenant la société qui édite et héberge le logiciel semble avoir trouvé un modèle économique viable grâce aux formations et à un module pour les professionnels du circuit court, et propose donc d'héberger gratuitement votre AMAP ou votre groupement d'achat.

