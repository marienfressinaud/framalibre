---
nom: "Galène"
date_creation: "Mardi, 2 mars, 2021 - 15:41"
date_modification: "Vendredi, 25 mars, 2022 - 12:55"
logo:
    src: "images/logo/Galène.png"
site_web: "https://galene.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "le web"
langues:
    - "English"
description_courte: "Galène est un logiciel de vidéoconférence léger et facile à déployer."
createurices: "Juliusz Chroboczek"
alternative_a: "Zoom, Skype"
licences:
    - "Licence MIT/X11"
tags:
    - "visioconférence"
    - "chat"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gal%C3%A8ne"
---

Galène (parfois aussi orthographié Galene) est un logiciel de vidéoconférence léger et facile à déployer.  Initialement conçu pour les cours et les conférences (où une personne communique avec des centaines ou des milliers de personnes), Galène s'est aussi avérée utile pour des réunions (où une vingtaine de personnes communiquent entre elles) et les travaux dirigés (où les gens communiquent en petits groupes de deux ou trois personnes).

