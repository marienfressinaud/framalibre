---
nom: "Mastodon"
date_creation: "Vendredi, 6 janvier, 2017 - 16:59"
date_modification: "Mercredi, 7 août, 2019 - 12:37"
logo:
    src: "images/logo/Mastodon.png"
site_web: "https://joinmastodon.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un logiciel décentralisé et fédéré de microblogage en 500 caractères."
createurices: "Eugen Rochko"
alternative_a: "Twitter"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "réseau social"
    - "décentralisation"
    - "microblog"
    - "fediverse"
    - "activitypub"
    - "ostatus"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Mastodon_(r%C3%A9seau_social)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/mastodon"
---

Mastodon est un logiciel de microblogage en 500 caractères créé en 2016.
Mastodon est un logiciel décentralisé : il existe de nombreuses instances sur lesquelles les utilisateurs peuvent s'inscrire. Chaque instance peut personnaliser ses conditions d'utilisation et ses fonctionnalités.
Les différentes instances de Mastodon communiquent entre elles pour échanger les messages des utilisateurs.
Des options d'import et d'export de données permettent à l'utilisateur de changer d'instance s'il le souhaite.
Grâce à ActivityPub et OStatus, Mastodon est un logiciel fédéré et fait donc partie du Fediverse. Il peut ainsi interagir avec d'autres logiciels comme GNU social ou PeerTube.
Parmi les nombreuses instances en fonctionnement on citera à titre d'exemple l'instance historique mastodon.social, framapiaf.org (hébergé par Framasoft) ou mamot.fr (La Quadrature du Net).

