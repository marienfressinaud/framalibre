---
nom: "GPodder"
date_creation: "Vendredi, 7 avril, 2017 - 21:58"
date_modification: "Lundi, 6 mai, 2019 - 09:39"
logo:
    src: "images/logo/GPodder.jpg"
site_web: "http://gpodder.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Ce logiciel vous permettra de suivre vos podcast ainsi que vos chaines youtube favorites."
createurices: "Thomas Perl"
alternative_a: "iTunes"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "podcast"
    - "téléchargement de youtube"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GPodder"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gpodder"
---

Gpodder est un lecteur et gestionnaire de podcast, tous comme AntennaPod sous Android .
Il vous permettra de suivre avec assiduité des millions d’émissions radiophoniques ou vidéos qu'elles soient professionnelles ou amateurs.
Mieux encore, grâce à lui vous pourrez automatiser le téléchargement et le visionnage des vidéos de toutes vos chaînes Youtube favorites et ce, sans avoir de compte Google
Alors qu'attendez vous pour l'essayer ?

