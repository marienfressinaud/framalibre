---
nom: "Vikunja"
date_creation: "Lundi, 11 octobre, 2021 - 23:14"
date_modification: "Lundi, 11 octobre, 2021 - 23:15"
logo:
    src: "images/logo/Vikunja.png"
site_web: "https://vikunja.io/"
plateformes:
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Organiser ses tâches sous forme de listes, de gantt chart, de tableaux ou de kanban, seul ou à plusieurs"
createurices: "kolaente"
alternative_a: "Todoist, Wunderlist, Microsoft To-Do"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "bureautique"
    - "todo-list"
    - "kanban"
    - "gestion de tâches"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/vikunja"
---

Vikunja est une application auto-hébergeable (facilement installable avec Yunohost) de gestion de tâches, utilisable depuis son navigateur ou un client desktop. Bien que jeune, l'application offre déjà de nombreuses fonctionnalités. Parmi celles-ci, citons :
la possibilité d'enrichir les tâches de nombreuses méta-données (tags, date d'échéance, pourcentage d'avancement, commentaires, etc.)
la saisie rapide de ces méta-données lors de la création d'une tâche (par exemple, !5 définit la priorité la plus haute)
les multiples façons de visualiser ses tâches : sous forme de listes, de tableaux, de Gantt chart ou de Kanban
la possibilité de collaborer à plusieurs
la création de filtres personnalisés
L'utilisateur peut créer différents "Espaces de nom" (par exemple : "Perso" et "Pro") qui contiendront des listes, qui elles-mêmes accueilleront les tâches.
Vikunja est donc une app très prometteuse, et déjà pleinement utilisable.

