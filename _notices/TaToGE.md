---
nom: "TaToGE"
date_creation: "Jeudi, 18 juillet, 2019 - 15:37"
date_modification: "Jeudi, 18 juillet, 2019 - 15:52"
logo:
    src: "images/logo/TaToGE.png"
site_web: "https://quasart.github.io/TaToGE/web"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
    - "English"
description_courte: "TaToGE (TableTop Game Equipments) : dés, sabliers, et autres matériels de jeux simulés sur un écran simple."
createurices: "Alfred Massard"
alternative_a: "tabletop simulator"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "jeu de société"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tatoge"
---

TaToGE (pour TableTop Game Equipments) est une application permettant de simuler un ensemble d’accessoires de jeux de société : dés, sabliers, compteurs. L'application permet de les afficher simplement sur un écran épuré afin d'accompagner les joueurs lors d'une partie "réelle".
L'application existe sous deux formes :
- TaToGE web/mobile, qui s'execute directement dans le navigateur.
- Une version de bureau en Qt, téléchargeable pour windows ici.

