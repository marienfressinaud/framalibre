---
nom: "NVDA"
date_creation: "Vendredi, 21 juin, 2019 - 10:42"
date_modification: "Dimanche, 21 juillet, 2019 - 18:17"
logo:
    src: "images/logo/NVDA.png"
site_web: "https://www.nvaccess.org/about-nvda"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "La meilleure solution libre pour l'accessibilité de l'informatique aux aveugles et malvoyants sous Windows"
createurices: "NV Access"
alternative_a: "JAWS, Narrateur Windows"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "lecteur d'écran"
    - "accessibilité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/NonVisual_Desktop_Access"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/nvda"
---

NVDA (Non Visual Desktop Access) est un lecteur d'écran : il permet aux utilisateurs aveugles ou malvoyants d'utiliser pleinement les fonctionnalités d'un ordinateur sous Windows, grâce à une synthèse vocale ou à un afficheur Braille, et à l'exploitation des outils d'accessibilité du système.
Ce logiciel supporte de nombreux modules, différents synthétiseurs et est disponible dans un grand nombre de langues.
Il existe une version portable pouvant être exécutée depuis une clé USB, afin de rendre accessible un ordinateur où NVDA n'est pas installé.

