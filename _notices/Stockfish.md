---
nom: "Stockfish"
date_creation: "Jeudi, 30 mars, 2023 - 14:06"
date_modification: "Jeudi, 30 mars, 2023 - 22:05"
logo:
    src: "images/logo/Stockfish.png"
site_web: "https://stockfishchess.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "L'un des meilleurs programmes pour jouer aux échecs."
createurices: "Tord Romstad, Marco Costalba, Joona Kiiski"
alternative_a: "Rybka, Komodo, Fritz"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Stockfish_(programme_d%27%C3%A9checs)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/stockfish"
---

Stockfish est un moteur de jeu d'échecs qui fonctionne en lignes de commandes. Il peut être utilisé avec de nombreux logiciels qui disposent d'une interface de jeu (Chess GUI, Graphical User Interface).
Stockfish est le meilleur logiciel d'échecs non commercial au monde depuis mai 2014. Sur l'ensemble des logiciels, il est généralement considéré comme le plus puissant des moteurs, bien que des alternatives commerciales le rivalisent.
Le logiciel détient le record historique mondial de points au classement Elo avec plus de 3 500 points (en mai 2021).

