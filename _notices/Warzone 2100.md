---
nom: "Warzone 2100"
date_creation: "Jeudi, 12 janvier, 2017 - 14:14"
date_modification: "Mercredi, 12 mai, 2021 - 16:55"
logo:
    src: "images/logo/Warzone 2100.png"
site_web: "http://wz2100.net/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Recherchez de nouvelles technologies et concevez vos propres véhicules dans ce jeu de stratégie en temps réel."
createurices: "Pumpkin Studio, Pivotal Games"
alternative_a: "Starcraft, Command & Conquer"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "jeu vidéo"
    - "stratégie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Warzone_2100"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/warzone-2100"
---

Warzone 2100 est un ancien jeu de stratégie en temps réel, développé par Pumpkin Studio et Pivotal Games, publié par Eidos en 1999. En 2004 le jeu est publié sous licence GPLv2.
Warzone 2100 est un jeu très axé sur les unités de véhicules et la balistique. Sa particularité réside dans le fait que le joueur doive concevoir lui même les véhicules qui constitueront son armée. L'arbre de recherche comprend plus de 400 technologies à rechercher permettant ainsi des milliers de combinaisons possibles.
Sa campagne solo est également très originale par rapport aux autres jeux du genre. En effet toutes les unités, ainsi que leur expérience sont conservées d'une mission à une autre.
Warzone 2100 supporte aussi les parties multijoueurs en LAN ou par internet.

