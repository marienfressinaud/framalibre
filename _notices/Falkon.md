---
nom: "Falkon"
date_creation: "Mardi, 12 juin, 2018 - 22:46"
date_modification: "Samedi, 1 juillet, 2023 - 14:48"
logo:
    src: "images/logo/Falkon.png"
site_web: "https://www.falkon.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un navigateur Web libre léger et multiplate-forme, successeur de QupZilla."
createurices: "David Rosca"
alternative_a: "Internet Explorer, Google Chrome, Opera, Qupzilla"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "navigateur web"
    - "web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Falkon"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/falkon"
---

Falkon est un navigateur web destiné au grand public. Il a pour vocation une intégration poussée aux environnements de bureau des utilisateurs mais propose également divers thèmes. Il utilise par défaut le moteur de recherche DuckDuckGo et intègre un bloqueur de publicité.
Il est développé en partant de l'idée que « léger » ne veut pas forcément dire « manque de fonctionnalités ».
Depuis la version 3.01, Falkon remplace
Konqueror comme navigateur par défaut de KDE.

