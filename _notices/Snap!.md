---
nom: "Snap!"
date_creation: "Lundi, 9 janvier, 2017 - 15:01"
date_modification: "Samedi, 2 juillet, 2022 - 11:31"
logo:
    src: "images/logo/Snap!.png"
site_web: "https://snap.berkeley.edu/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
langues:
    - "Autres langues"
description_courte: "Un langage de programmation fonctionnel, visuel, ludique, simple d'accès et aux possibilités illimitées..."
createurices: "Jens Mönig, Brian Harvey"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "éducation"
    - "programmation"
    - "apprentissage"
    - "enfant"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Snap!_(langage)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/snap"
---

A l'instar de Scratch, Snap! offre une interface visuelle de programmation par cliqué-déposé de blocs. Il intéresse tout particulièrement le monde de l'éducation pour apprendre à créer des animations interactives, des jeux, des simulations, des histoires, et enseigner les maths et l'informatique.
Snap! permet de définir ses propres blocs sur le modèle de fonctions. Il est extrêmement riche et évolutif. Snap! manipule des listes et des arbres, implémente les continuations, autorise à passer des objets en arguments, gère des appels en http, etc.
Selon son créateur, Snap! offre différents niveaux d'accès:
low floor accessible très facilement au plus grand nombre,
no ceiling on peut faire des choses très, très complexes (cf lambda calcul)
wide walls plein d'approches différentes sont possibles.
Des extensions sont développées pour l'interfacer notamment avec Arduino ou pour étendre encore ses capacités: graphes, multi-agents, art génératif, coopération....

