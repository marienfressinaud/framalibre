---
nom: "NewPipe"
date_creation: "Jeudi, 12 avril, 2018 - 10:07"
date_modification: "Lundi, 9 novembre, 2020 - 10:21"
logo:
    src: "images/logo/NewPipe.png"
site_web: "https://newpipe.schabi.org/"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Client multimédia YouTube, PeerTube, SoundCloud & MediaCCC libre pour Android."
createurices: "Christian Schabesberger"
alternative_a: "Youtube, TubeMate, Soundcloud"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "téléchargement de youtube"
    - "android"
    - "lecteur vidéo"
    - "lecteur musique"
    - "peertube"
lien_wikipedia: "https://fr.wikipedia.org/wiki/NewPipe"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/org.schabi.newpipe/latest/"
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/newpipe"
---

Pas besoin des services Google installés sur votre téléphone.
Permet de lire les vidéos, de s'abonner à des chaînes (sans compte), de télécharger les vidéos et beaucoup d'autres choses encore !
Disponible sur F-Droid.

