---
nom: "Flatisfy"
date_creation: "Mardi, 27 novembre, 2018 - 10:28"
date_modification: "Mardi, 27 novembre, 2018 - 10:28"
logo:
    src: "images/logo/Flatisfy.png"
site_web: "https://framagit.org/phyks/Flatisfy"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "English"
description_courte: "Votre nouveau compagnon pour vous aider à trouver votre futur logement !"
createurices: "Phyks"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "achat"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/flatisfy"
---

Flatisfy est votre compagnon pour vous accompagner dans la recherche de votre futur logement. Il utilise WebOOB pour récupérer automatiquement les annonces correspondant à vos critères de recherche sur les sites d'annonces immobilières. Il passe ensuite les annonces à travers une chaîne de traitements pour extraire et standardiser les informations pertinentes et tenter d'éliminer autant de doublons que possible.
Flatisfy est écrit en Python et prévu pour fonctionner sur un serveur. Une partie visualisation web est disponible afin de parcourir les annonces récupérées et garder trace de vos annonces préférées et des visites programmées.

