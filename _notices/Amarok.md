---
nom: "Amarok"
date_creation: "Mardi, 3 janvier, 2017 - 14:19"
date_modification: "Vendredi, 13 août, 2021 - 17:48"
logo:
    src: "images/logo/Amarok.png"
site_web: "https://amarok.kde.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un lecteur audio très complet et extensible."
createurices: ""
alternative_a: "iTunes, Windows Media Player"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "audio"
    - "musique"
    - "lecteur audio"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Amarok_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/amarok"
---

Bien plus qu'un simple lecteur audio, Amarok est un gestionnaire de bibliothèque musicale aux très nombreuses fonctionnalités.
Il permet aussi bien d'utiliser une discothèque locale, de lire des CD audio ou d’accéder de manière transparente à de nombreux services en ligne comme Ampache, Jamendo, Magnatunes, LastFM, Librivox, des services de podcast, etc.
Amarok permet de trier et d'organiser vos morceaux de musique comme bon vous semble à partir des marqueurs présents. Il peut télécharger les pochettes des albums, afficher les paroles des chansons ou la page Wikipédia de l’artiste, créer ou générer des listes de lecture et bien d’autres encore.
Ce logiciel propose de nombreux modules externes ou scripts qui permettent d’étendre encore plus ses capacités.

