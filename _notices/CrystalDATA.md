---
nom: "CrystalDATA"
date_creation: "Jeudi, 2 novembre, 2023 - 15:11"
date_modification: "Mardi, 21 novembre, 2023 - 11:33"
logo:
    src: "images/logo/CrystalDATA.png"
site_web: "https://crystal-data.philnoug.com"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
description_courte: "Gestionnaire d'informations NoCode"
createurices: "Philippe NOUGAILLON"
alternative_a: "Microsoft Access, Filemaker, Airtable"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "nocode"
    - "base de données"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/crystaldata"
---

Application NO-CODE de base de données en ligne, qui vous permet de créer des objets métiers variés (Intervention, Technicien, Frais, Article, etc.), de les lier entre eux (Interventions 
