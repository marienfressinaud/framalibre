---
nom: "Contrast Finder"
date_creation: "Mercredi, 22 mars, 2017 - 23:34"
date_modification: "Jeudi, 6 mai, 2021 - 12:14"
logo:
    src: "images/logo/Contrast Finder.png"
site_web: "https://contrast-finder.org"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Logiciel pour valider le contraste entre 2 couleurs et proposer des couleurs alternatives (accessibilité web)."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "création"
    - "couleur"
    - "accessibilité"
    - "wcag"
    - "rgaa"
    - "java"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/contrast-finder"
---

Contrast-Finder permet de valider le contraste entre 2 couleurs et propose, si besoin, des couleurs alternatives adaptées aux règles d'accessibilité du web (WCAG, RGAA).
Les personnes ayant du mal à distinguer les couleurs liront difficilement vos pages web, si le contraste (en teinte ou en luminosité) est trop faible entre le texte et l'arrière-plan.
Dans le cas où le contraste n'est pas suffisant, Contrast-Finder vous  suggère une palette de couleurs valides grâce à 2 algorithmes : un mode rapide  et un mode où les couleurs proposées sont très proches de la couleur initiale.
Ce logiciel s'installe avec un serveur Tomcat.  Une image Docker de Contrast-Finder est aussi disponible pour le tester rapidement sur votre ordinateur.

