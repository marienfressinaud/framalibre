---
nom: "emailPoubelle"
date_creation: "Mardi, 4 avril, 2017 - 23:19"
date_modification: "Lundi, 10 mai, 2021 - 13:40"
logo:
    src: "images/logo/emailPoubelle.png"
site_web: "http://poubelle.zici.fr/"
plateformes:
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
description_courte: "Mettez en place un système d'emails poubelles sur votre serveur personnel pour dire adieu au spam..."
createurices: "David Mercereau"
alternative_a: "jetable.org, crazymailing.com, yopmail.com, tempomail, spamgourmet"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "client mail"
    - "auto-hébergement"
    - "email jetable"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/emailpoubelle"
---

Vous avez besoin d’un service comme jetable.org, mail-temporaire.fr…  emailPoubelle.php c’est pareil… mais en mieux (parce que « à la maison »)
emailPoubelle.php est donc une page php qu'il est possible d'inclure dans votre site. Elle permet d’administrer des adresses email (des alias plus précisément) jetable ou temporaire. Ces adresses peuvent avoir une durée de vie limitée ou non. Elles peuvent être désactivées (>/dev/null) à tout moment par l'utilisateur.

