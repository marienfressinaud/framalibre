---
nom: "LightBulb"
date_creation: "Vendredi, 5 mai, 2017 - 00:06"
date_modification: "Mercredi, 12 mai, 2021 - 14:44"
logo:
    src: "images/logo/LightBulb.png"
site_web: "https://github.com/Tyrrrz/LightBulb"
plateformes:
    - "Windows"
langues:
    - "English"
description_courte: "Application de fond qui réduit la fatigue oculaire la nuit en ajustant automatiquement l'affichage gamma."
createurices: "Alexey Golub, Tyrrrz"
alternative_a: "f.lux, Twilight"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "utilitaire"
    - "colorimétrie"
    - "yeux"
    - "vision"
    - "écran"
    - "lumière"
    - "couleur"
    - "santé"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lightbulb"
---

La production de mélatonine peut être inhibée par la lumière bleu artificielle de nos écrans et ainsi agir sur la qualité du sommeil.
Cette application ajuste automatiquement l'affichage gamma et la température couleur de votre écran selon l'heure de la journée, Dominante bleue en journée, dominante rouge en soirée/nuit.

