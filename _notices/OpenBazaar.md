---
nom: "OpenBazaar"
date_creation: "Mardi, 14 mars, 2017 - 22:04"
date_modification: "Mercredi, 12 mai, 2021 - 15:41"
logo:
    src: "images/logo/OpenBazaar.png"
site_web: "https://openbazaar.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "OpenBazaar est une nouvelle façon de faire du commerce en ligne grâce au p2p et au bitcoin."
createurices: "Amir Taaki"
alternative_a: "Amazon"
licences:
    - "Licence MIT/X11"
tags:
    - "internet"
    - "commerce"
    - "p2p"
    - "décentralisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OpenBazaar"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openbazaar"
---

OpenBazaar propose une nouvelle façon de faire du commerce en ligne grâce au p2p. Au lieu de visiter un site internet vous téléchargez le programme sur votre ordinateur qui se connecte aux personnes qui cherchent à acheter des marchandises ou qui proposent les leurs. OpenBazaar c'est une communauté qui ne dépends d'aucune organisation. L'application concerne donc les personnes qui veulent s'engager dans le commerce sans intermédiaires.

