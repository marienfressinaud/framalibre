---
nom: "Xubuntu"
date_creation: "Vendredi, 19 juillet, 2019 - 23:19"
date_modification: "Jeudi, 20 mai, 2021 - 16:27"
logo:
    src: "images/logo/Xubuntu.png"
site_web: "http://xubuntu.fr/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Une variante d'Ubuntu légère et réactive !"
createurices: ""
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Multiples licences"
tags:
    - "système"
    - "distribution gnu/linux"
    - "système d'exploitation (os)"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Xubuntu"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/xubuntu"
---

Xubuntu est un système d'exploitation libre dérivé d'Ubuntu. Il bénéficie des mêmes qualités en termes de facilité d'installation et d'utilisation.
Utilisant l'environnement de bureau Xfce (et non Gnome comme Ubuntu), Xubuntu est un système d'exploitation  plus léger et donc plus réactif, ce qui est un atout appréciable si votre ordinateur n'est pas particulièrement performant.

