---
nom: "FreshRSS"
date_creation: "Samedi, 21 janvier, 2017 - 15:45"
date_modification: "Mercredi, 12 mai, 2021 - 16:40"
logo:
    src: "images/logo/FreshRSS.png"
site_web: "https://freshrss.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Apple iOS"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "FreshRSS permet de s'abonner à des sites pour suivre l'actualité."
createurices: "Marien Fressinaud, Alexandre Alapetite"
alternative_a: "Google Reader, Feedly, Netvibes"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "agrégateur"
    - "lecteur de flux rss"
    - "atom"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FreshRSS"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/freshrss"
---

FreshRSS est un agrégateur de flux RSS et Atom à héberger sur un serveur. Il permet de s'abonner à des sites pour recevoir les articles aussi tôt qu'ils sont publiés.
Il permet de stocker les articles pour les lire plus tard et conserver une liste de favoris. FreshRSS permet de faire des recherches et créer des filtres à partir de celles-ci.
FreshRSS dispose d'une API basée sur celle de feu Google Reader ce qui le rend compatible avec des applications Android comme EasyRSS.

