---
nom: "Wings 3D"
date_creation: "Dimanche, 12 mars, 2017 - 02:03"
date_modification: "Mercredi, 12 mai, 2021 - 15:53"
logo:
    src: "images/logo/Wings 3D.png"
site_web: "http://www.wings3d.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Wings 3D est un logiciel de modélisation 3D. La prise en main se veut simple et intuitive."
createurices: "Wings Team"
alternative_a: "Autodesk 3ds Max"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "création"
    - "3d"
    - "modélisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Wings_3D"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/wings-3d"
---

Inspiré de Nendo et Mirai de la société Izware, Wings 3D est un logiciel de modélisation codé en Erlang. Il se limite à la modélisation et à l'application de textures. Ce n'est donc pas un logiciel 3D complet comme l'est Blender, cependant il est très accessible ce qui rend la modélisation ludique et rapide.

