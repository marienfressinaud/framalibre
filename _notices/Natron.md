---
nom: "Natron"
date_creation: "Dimanche, 12 mars, 2017 - 10:27"
date_modification: "Mercredi, 12 mai, 2021 - 15:43"
logo:
    src: "images/logo/Natron.png"
site_web: "https://natrongithub.github.io/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Natron est un logiciel de post-production vidéo (composition et effets spéciaux)."
createurices: "Alexandre Gauthier"
alternative_a: "Adobe After Effects, The Foundry Nuke, BlackMagic Design Fusion"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "vidéo"
    - "montage vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Natron_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/natron"
---

Natron est un logiciel de post-production vidéo (composition et effets spéciaux) nodal libre et open source. Il est largement inspiré de Nuke, dont il reprend de nombreux concepts et s'inspire pour son interface.
Natron supporte les plugins OpenFX 1.4, la plupart des plugins open-source et commerciaux sont supportés.
L'interface n'est disponible que en anglais, tout comme la documentation.

