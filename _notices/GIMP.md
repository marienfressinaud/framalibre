---
nom: "GIMP"
date_creation: "Mardi, 3 février, 2015 - 22:54"
date_modification: "Jeudi, 13 avril, 2017 - 21:59"
logo:
    src: "images/logo/GIMP.png"
site_web: "https://www.gimp.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un puissant logiciel d'édition et de retouche d'images."
createurices: "Spencer Kimball, Peter Mattis"
alternative_a: "Adobe Photoshop, Adobe Illustrator, Paint.net"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "dessin"
    - "image"
    - "graphisme"
    - "infographie"
    - "photo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GIMP"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/gimp"
---

GIMP (GNU Image Manipulation Program) est un logiciel d'édition et de retouche d'image. De nombreuses extensions sont disponibles et le logiciel possède de nombres fonctionnalités hautement paramétrables. Il est possible d'utiliser GIMP pour un usage quotidien comme dans un environnement professionnel.

