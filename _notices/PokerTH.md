---
nom: "PokerTH"
date_creation: "Vendredi, 7 avril, 2017 - 23:05"
date_modification: "Vendredi, 7 avril, 2017 - 23:05"
logo:
    src: "images/logo/PokerTH.png"
site_web: "https://www.pokerth.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Jouer au poker oui, mais avec un logiciel libre !"
createurices: "Felix Hammer, Florian Thauer"
alternative_a: "Winamax"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "jeu"
lien_wikipedia: "https://fr.wikipedia.org/wiki/PokerTH"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pokerth"
---

Un jeu de Texas Hold'em pour jouer au poker en solo ou en réseau contre un maximum de 10 adversaires.  Il dispose de plusieurs ajouts tels que des effets sonores, la présence et le choix des avatars, ainsi que le choix possible du set de cartes à jouer.
Amusez vous bien !

