---
nom: "Avogadro"
date_creation: "Mardi, 11 juillet, 2017 - 21:24"
date_modification: "Mardi, 11 juillet, 2017 - 21:47"
logo:
    src: "images/logo/Avogadro.png"
site_web: "https://avogadro.cc/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Éditeur et visualiseur de molécules chimiques en 3D."
createurices: "https://avogadro.cc/credits/"
alternative_a: "Symyx Draw, MDL ISIS Draw, BIOVIA Draw, ChemDraw"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "dessin"
    - "schéma"
    - "modélisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Avogadro_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/avogadro"
---

Avogadro est à la fois un éditeur et visualiseur de molécules en trois dimensions. C'est à dire que l'on va pouvoir visualiser les fichiers moléculaires disponibles un peu partout sur internet (tous les formats connus de modélisation de molécules sont reconnus par le logiciel) mais également les modifier ou en créer d'autres ex-nihilo.
C'est vraiment l'outil par excellence pour visualiser une molécule en la déplaçant en 3D directement sur l'écran dans Avogadro ou en enregistrant au préalable un petit film de la molécule en rotation dans l'espace pour la contempler sous toutes ses coutures.

