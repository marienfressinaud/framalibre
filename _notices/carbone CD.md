---
nom: "carbone CD"
date_creation: "Lundi, 9 avril, 2018 - 13:58"
date_modification: "Mardi, 11 mai, 2021 - 23:13"
logo:
    src: "images/logo/carbone CD.png"
site_web: "https://www.rizonesoft.com/downloads/carbon-cd/"
plateformes:
    - "Windows"
langues:
    - "English"
description_courte: "Utilisez Carbon CD pour effectuer des copies de sauvegarde de vos CD de musique ou de données."
createurices: "Derick Payne"
alternative_a: "clone CD"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "cd-rom"
    - "gravure"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/carbone-cd"
---

Copiez (Cloner) la plupart des CD en quelques clics de souris. Créez des copies parfaites en 1: 1 ou sauvegardez vos CD sur votre ordinateur. Si vous êtes familier avec Clone CD, vous devriez vous sentir comme chez vous en utilisant Carbon CD. La plus grande différence entre les deux est que Carbon CD ne supporte pas encore les DVD
Caractéristiques générales
    Créez des copies 1: 1 sur CD-R et CD-RW.
    Formats pris en charge: ISO, IMG, CUE, CCD, CDM
    Ne nécessite pas de pilote ASPI.
    Afficher les capacités du lecteur.
    MSF Calculatrice.
    Effacer le support CD-RW.
    Masterisation de CD et de fichiers image.
    Extraire les pistes audio.
    Copier Protéger les CD.
Fonctions de lecture
  Copie les CD en mode Session unique ou en mode Multi-Session (RAW).
Analyser pregap.
Analyser le sous-code.
Ignorer les erreurs de lecture.
Saut d'erreur rapide.
Scanner intelligent du secteur défectueux.
Échange de canal audio.

