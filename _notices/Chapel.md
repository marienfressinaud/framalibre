---
nom: "Chapel"
date_creation: "Samedi, 18 juillet, 2020 - 15:18"
date_modification: "Samedi, 18 juillet, 2020 - 15:18"
logo:
    src: "images/logo/Chapel.png"
site_web: "https://chapel-lang.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Un outil de nouvelle génération pour la programmation parallèle, sur super calculateur ou machine seule."
createurices: ""
alternative_a: "Matlab"
licences:
    - "Licence Apache (Apache)"
tags:
    - "science"
    - "calcul numérique"
lien_wikipedia: "https://en.wikipedia.org/wiki/Chapel_(programming_language)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/chapel"
---

Chapel est un langage de programmation qui a pour but d'accélérer le développement des programmes parallèles. Ainsi, moyennant une installation du compilateur relativement technique sur une architecture distribuée (super ordinateur), Chapel permet de développer de manière concise et efficace des programmes prêts à tourner en parallèle sur plusieurs machines reliées localement. Il est également possible de l'utiliser sur un seul ordinateur (l'installation du compilateur se fait encore en compilant depuis le code source de ce dernier mais sans difficultées particulières), même si dans ce cas les performances restent bonnes mais inférieures à la programmation en C ou Fortran (mais avec une synthaxe à peu près aussi simple que Octave/Matlab/SciLab). En bref, un langage qui devrait intéresser toute personne concernée par le calcul à haute performance.

