---
nom: "Friendica"
date_creation: "Samedi, 23 février, 2019 - 09:35"
date_modification: "Mercredi, 24 juillet, 2019 - 20:53"
logo:
    src: "images/logo/Friendica.png"
site_web: "https://friendi.ca/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Réseau social décentralisé et fédéré"
createurices: "Indépendants"
alternative_a: "Facebook, Hubzilla, Diaspora"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "réseau social"
    - "décentralisation"
    - "activitypub"
    - "ostatus"
    - "fediverse"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Friendica"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/friendica"
---

Friendica est un logiciel libre qui implémente un réseau social distribué, comme Diaspora, Mastodon ou GNU Social. Il est également fédéré, et donc permet de suivre ces plateformes et d'y publier depuis son compte Friendica. Il permet également d'écrire à des adresses courriel, de suivre des flux RSS, et est extensible avec des extensions.
Friendica permet de choisir la visibilité de nos actions de manière fine (publique, privée, restreinte à des groupes, etc) et comporte des options uniques quant au respect de la vie privée. Par exemple, on peut définir une date d'expiration du contenu, et on peut télécharger toutes ses données personnelles.
Friendica est toujours en développement actif, mais notons l'existence de Hubzilla qui est un projet dans la continuité de Friendica.

