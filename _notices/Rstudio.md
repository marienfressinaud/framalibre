---
nom: "Rstudio"
date_creation: "Vendredi, 13 février, 2015 - 09:23"
date_modification: "Jeudi, 20 juillet, 2017 - 13:39"
logo:
    src: "images/logo/Rstudio.png"
site_web: "https://www.rstudio.com"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Rstudio est un environnement de développement pour R."
createurices: ""
alternative_a: "SAS, eviews, Microsoft Excel, Stata"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "science"
    - "traitement de données"
    - "analyse statistique"
    - "économie"
    - "géographie"
    - "sociologie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/RStudio"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/rstudio"
---

Rstudio est un environnement de développement pour R. Il ne s'agit pas d'une interface graphique mais d'un environnement facilitant la saisie de code, l’exécution, le retour des résultats, etc. La coloration syntaxique, l'historique des commandes et l'autocomplétion permettent d'appréhender R de manière efficace et productive. Rstudio peut s'utiliser localement et une version serveur est aussi disponible.

