---
nom: "OpenWrt"
date_creation: "Mercredi, 17 octobre, 2018 - 21:35"
date_modification: "Mercredi, 17 octobre, 2018 - 21:35"
logo:
    src: "images/logo/OpenWrt.png"
site_web: "https://openwrt.org/"
plateformes:
    - "Autre"
langues:
    - "Autres langues"
description_courte: "OpenWrt est un firmware libre pour routeur"
createurices: ""
alternative_a: "Linksys, LEDE, pfsense, OPNsense"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "internet"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OpenWrt"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openwrt"
---

OpenWrt permet de remplacer le firmware de certains routeurs grand public. Par défaut ce firmware vient avec une interface graphique et l'ensemble des fonctionnalités que l'on peut attendre d'un routeur grand public (gestion du Wi-Fi, DHCP, etc) et quelques fonctionnalités plus avancés tels que des graphiques d'utilisations réseau. Une gestionnaire de paquet embarqué permet d'augmenté grandement les possibilités.

