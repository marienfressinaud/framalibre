---
nom: "markPush"
date_creation: "Mardi, 27 juin, 2023 - 16:14"
date_modification: "Mardi, 27 juin, 2023 - 16:14"
logo:
    src: "images/logo/markPush.png"
site_web: "https://codeberg.org/jrm-omg/markPush"
plateformes:
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
description_courte: "Transforme tes fichiers markdown en site web"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "cms"
    - "php"
    - "création site web"
    - "développement web"
    - "markdown"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/markpush"
---

markPush permet de convertir des tas de fichiers Markdown en fichiers HTML. Sa force réside dans le fait qu'il est capable de s'adapter à n'importe quelle structure de dossiers, fichiers et sous-dossiers. Un peu comme GitHub pages, mais sans GitHub.
Tout ce dont vous avez besoin c'est de PHP pour exécuter le script markPush.php

