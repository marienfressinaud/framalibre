---
nom: "Meteor"
date_creation: "Mercredi, 22 mars, 2017 - 09:26"
date_modification: "Lundi, 10 mai, 2021 - 14:40"
logo:
    src: "images/logo/Meteor.png"
site_web: "https://www.meteor.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
    - "Autres langues"
description_courte: "Coté client ET coté serveur : Tout est en JS & Package NPM
Coté client : Choisissez React, Blaze ou Angular"
createurices: "Geoff Schmidt, Matt DeBergalis, Nick Martin"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "système"
    - "environnement de développement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Meteor_(framework)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/meteor"
---

Meteor permet de développer avec le même langage (en Javascript ou dans un langage compilant vers Javascript comme CoffeeScript ou Dart) et avec la même API sur le client et sur le serveur. Ce choix d’architecture permet de déplacer facilement un traitement du serveur vers le client (et réciproquement) voire de le dupliquer par exemple dans le cas de la validation d'un formulaire.
Dans cette logique, Meteor inclut un système de gestion de base de données côté client (un "MongoDB" ultraléger), fonctionnalité originale du framework. Il est ainsi possible d'effectuer des requêtes même en étant déconnecté du serveur. Cela permet notamment à Meteor d'inclure par défaut, des mécanismes de compensation de latence. Par exemple, l'envoi d'un message dans un chat sera instantanément ajouté au fil des messages au clic sur le bouton "Envoyer", tandis que la vérification du message se fera en arrière plan côté serveur. Ce mécanisme permet l'utilisation de la programmation réactive coté client.

