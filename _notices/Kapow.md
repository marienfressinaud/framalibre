---
nom: "Kapow"
date_creation: "Lundi, 5 novembre, 2018 - 11:49"
date_modification: "Lundi, 10 mai, 2021 - 14:56"
logo:
    src: "images/logo/Kapow.png"
site_web: "https://gottcode.org/kapow/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Gérez votre temps facilement !"
createurices: "Graeme Gott"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "gestion du temps"
    - "gestion de projet"
    - "tâche"
    - "productivité"
    - "temps"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/kapow"
---

Kapow est un logiciel de suivi de temps de travail.
Son interface simple et ses fonctions épurées en rendent la prise en main très facile.
Il vous suffit de créer un projet pour l'activité sur laquelle vous souhaitez suivre votre temps de travail, de donner un nom à la tâche que vous êtes en train d'effectuer puis de lancer le chronomètre.
Kapow ne vous distraira pas pendant votre travail, une icône présente dans la barre des tâches une fois le logiciel lancé vous indique si le chronomètre est en route. Un clic sur cette icône suffit pour afficher/masquer complètement l'application.
Kapow vous permettra aussi de générer des rapports d'activités pour faciliter la facturation du temps passé sur le(s) projet(s) de votre choix.
Vous avez la possibilité de personnaliser un taux horaire et un taux de TVA.

