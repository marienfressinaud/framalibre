---
nom: "USB Disk Ejector"
date_creation: "Dimanche, 21 juillet, 2019 - 21:32"
date_modification: "Dimanche, 21 juillet, 2019 - 21:32"
logo:
    src: "images/logo/USB Disk Ejector.png"
site_web: "https://quickandeasysoftware.net/software/usb-disk-ejector"
plateformes:
    - "Windows"
langues:
    - "English"
description_courte: "Une façon plus commode d'éjecter les périphériques de stockage USB"
createurices: "Bgbennyboy"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "utilitaire"
    - "usb"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/usb-disk-ejector"
---

Si vous vous faites partie de ceux qui continuent à éjecter proprement les clés USB ou les disques durs externes avant de les débrancher, USB Disk Ejector pourrait vous plaire.
En effet, vous pouvez trouver fastidieux d'aller attraper à la souris la petite icône de Windows dédiée à cela. C'est là où USB Disk Ejector intervient : le programme ouvre une fenêtre qui affiche les périphériques et il suffit d'en sélectionner un pour l'éjecter.
J'ai moi-même associé USB Disk Ejector à un raccourci clavier et j'éjecte maintenant mes clés USB en un tour de main Bien plus rapide et pratique à mon goût !

