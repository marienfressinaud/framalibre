---
nom: "ART"
date_creation: "Mercredi, 22 mars, 2017 - 15:36"
date_modification: "Mercredi, 22 mars, 2017 - 15:36"
logo:
    src: "images/logo/ART.png"
site_web: "http://art.sourceforge.net"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "un outil simple de reporting"
createurices: ""
alternative_a: "Business Object"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "reporting"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/art"
---

ART est un outil de reporting en mode web, permettant d'afficher sous forme de listes ou de graphique les résultats de requêtes SQL paramétrées.
Les résultats peuvent être visualisés dans le navigateur, ou récupérés dans un tableur.
Cet outil est simple à mettre en place (c'est un paquet au format war, que l'on peut installer facilement dans n'importe quel conteneur web tel que tomcat) et à utiliser.

