---
nom: "Goozzee"
date_creation: "Vendredi, 5 mai, 2017 - 10:45"
date_modification: "Vendredi, 11 mai, 2018 - 05:48"
logo:
    src: "images/logo/Goozzee.png"
site_web: "http://goozzee.sourceforge.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Créez des bases de connaissances sous forme de \"cartes topiques\""
createurices: "Michel Laglasse"
alternative_a: "OneNote, personal brain"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "base de connaissances"
    - "documentation"
    - "organisation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/goozzee"
---

Goozzee est un logiciel permettant de créer des bases de connaissances.
Goozzee n'organise pas les données de façon hiérarchique, mais sous forme de "Topic Maps" -- ou cartes topiques --  ce qui permet une navigation et une recherche d'information très rapides.
L'écran principal permet de visualiser toutes les informations concernant un sujet donné : des notes (grâce à un éditeur de texte intégré), des documents (n'importe quel type de fichier peut être importé), des propriétés et des liens vers d'autres sujets.
Goozzee permet de définir l'ontologie d'un domaine de connaissance, sous forme d'une hiérarchie de classes/instance (avec héritage des propriétés et documents) et de contraintes entre les classes (c'est-à-dire les types de liens pouvant être créés entre les différentes classes).
Goozzee est disponible en version mono ou multi-utilisateur.

