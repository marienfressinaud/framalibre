---
nom: "Knowledge Manager"
date_creation: "Lundi, 27 avril, 2020 - 16:31"
date_modification: "Lundi, 27 avril, 2020 - 16:31"
logo:
    src: "images/logo/Knowledge Manager.png"
site_web: "https://www.mentdb.org/knowledge.html"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Mettez rapidement du savoir à disposition de tous dans votre entreprise."
createurices: "Jimmitry Payet"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/knowledge-manager"
---

Ce logiciel vous permettra de rassembler les connaissances de l'entreprise à un seul endroit. Une interface de recherche rapide existe et vous permettra de retrouver rapidement un savoir. La partie administration vous permettra de créer des catégories et des articles. A vous de décidé quand l'article sera prêt à être publier.

