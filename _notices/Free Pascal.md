---
nom: "Free Pascal"
date_creation: "Lundi, 13 mai, 2019 - 09:39"
date_modification: "Lundi, 10 mai, 2021 - 13:16"
logo:
    src: "images/logo/Free Pascal.gif"
site_web: "https://www.freepascal.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un compilateur léger pour le langage Pascal"
createurices: "Florian Klämpfl"
alternative_a: "Delphi"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "développement"
    - "langage"
    - "programmation"
    - "compilation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Free_Pascal"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/free-pascal"
---

Free Pascal est un compilateur léger pour le langage Pascal, écrit en C à l'origine. Il est reconnu pour disposer d'une grand compatibilité. Les projets Free Pascal et Lazarus ont coopéré en créant la Free Pascal and Lazarus Foundation, mise en valeur récemment. Lazarus (une alternative à Delphi) intègre Free Pascal.

