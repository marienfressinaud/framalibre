---
nom: "Syncloud"
date_creation: "Dimanche, 10 mars, 2019 - 13:51"
date_modification: "Jeudi, 17 août, 2023 - 21:08"
logo:
    src: "images/logo/Syncloud.png"
site_web: "https://syncloud.org/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un petit serveur pour auto-héberger ses données."
createurices: "Boris Rybalkin, Vladimir Sapronov"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "auto-hébergement"
    - "serveur"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/syncloud"
---

Syncloud est un petit serveur similaire à YunoHost et FreedomBox qui dispose d'une interface d'administration web et de quelques applications : Nextcloud, Diaspora, Roundcube, File browser, Rocket.Chat, GOGS git server, Syncthing et Wordpress.

