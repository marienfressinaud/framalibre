---
nom: "Air Board Game"
date_creation: "Mercredi, 3 février, 2021 - 22:50"
date_modification: "Mercredi, 12 mai, 2021 - 16:56"
logo:
    src: "images/logo/Air Board Game.png"
site_web: "https://airboardgame.net"
plateformes:
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Plateforme web permettant de jouer à des jeux de société en ligne !"
createurices: "Jérémie Pardou"
alternative_a: "Tabletopia, Boardgamearena, tabletop simulator"
licences:
    - "Licence MIT/X11"
tags:
    - "jeu"
    - "jeu de société"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/air-board-game"
---

Ce site web permet de jouer à des jeux de société en ligne, sans avoir besoin de créer un compte.
Plusieurs jeux sont déjà proposés et d'autres peuvent être ajoutés très facilement sans avoir besoin de compétence particulière. Commencez une partie, et partagez le lien avec vos amis.
Vous pouvez exporter une partie et la réimporter plus tard.
De plus la plateforme permet de créer vos propres jeux, et vous pouvez même utiliser un plateau pour animer un atelier en ligne… Bref, jouez bien !

