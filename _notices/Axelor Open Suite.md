---
nom: "Axelor Open Suite"
date_creation: "Vendredi, 10 mai, 2019 - 14:33"
date_modification: "Vendredi, 5 juillet, 2019 - 17:39"
logo:
    src: "images/logo/Axelor Open Suite.png"
site_web: "https://www.axelor.com/fr/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Axelor Open Suite, ERP Open Source"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métiers"
    - "erp"
    - "crm"
    - "saas"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/axelor-open-suite"
---

Axelor Open Suite est un ERP Open Source entièrement modulaire qui bénéficie de plus d'une vingtaine d'applications métiers intégrées : CRM, gestion des ventes et des achats, facturation, gestion à l'affaire, comptabilité, gestion des stocks et de la production, gestion des ressources humaines...
Axelor Open Suite est un des ERP les plus flexibles du marché grâce à son approche innovante et sa plateforme low code intégrée. La solution s'adapte aux besoins des entreprises en limitant au maximum les développements spécifiques longs et coûteux. Vous pouvez personnaliser graphiquement vos règles et processus métiers, ajouter ou modifier des champs grâce à de simples glisser-déposer.
Nativement web et entièrement responsive, vous pouvez accéder à toutes vos données sur smartphones et tablettes ainsi qu'à des applications mobiles spécifiques.
Axelor est une solution qui peut être souscrite dans sa version cloud ou on-premise.

