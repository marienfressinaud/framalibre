---
nom: "Leed"
date_creation: "Mercredi, 25 janvier, 2017 - 13:50"
date_modification: "Mardi, 1 juin, 2021 - 11:17"
logo:
    src: "images/logo/Leed.png"
site_web: "https://github.com/LeedRSS/Leed"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Leed (contraction de Light Feed) est un agrégateur RSS/ATOM minimaliste qui permet la consultation de flux RSS"
createurices: "Valentin CARRUESCO, Christophe HENRY, Simon ALBERNY, Maël ILLOUZ"
alternative_a: "Netvibes, Feedly, Google Reader"
licences:
    - "Creative Commons -By-Sa"
tags:
    - "lecteur de flux rss"
    - "atom"
    - "agrégateur"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/leed"
---

Leed (contraction de Light Feed) est un agrégateur RSS/ATOM minimaliste qui permet la consultation de flux RSS de manière rapide et non intrusive.
Cet agrégateur peut s'installer sur votre propre serveur et fonctionne avec un système de tâches cron afin de traiter les informations de manière transparente et de les afficher le plus rapidement possible lorsque vous vous y connectez.

