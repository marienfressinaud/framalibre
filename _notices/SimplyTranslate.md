---
nom: "SimplyTranslate"
date_creation: "Samedi, 4 juin, 2022 - 00:21"
date_modification: "Mercredi, 13 juillet, 2022 - 06:50"
logo:
    src: "images/logo/SimplyTranslate.png"
site_web: "https://pub.dev/packages/simplytranslate"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "English"
description_courte: "Un traducteur libre et plus respectueux de la vie privée."
createurices: ""
alternative_a: "Google Traduction, DeepL, Reverso"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "traduction"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/simplytranslate"
---

SimplyTranslate est un frontend qui peut regrouper des traducteurs comme Google Traduction, Libre Translate (qui est lui aussi un logiciel libre), Reverso, ICIBA et DeepL. Son interface est volontairement minimaliste  Il est écrit en Python.

