---
nom: "PuTTY"
date_creation: "Dimanche, 12 février, 2017 - 13:58"
date_modification: "Dimanche, 12 février, 2017 - 13:58"
logo:
    src: "images/logo/PuTTY.png"
site_web: "http://www.chiark.greenend.org.uk/~sgtatham/putty/"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Autre"
langues:
    - "English"
description_courte: "PuTTY est un émulateur de terminal doublé d'un client pour les protocoles SSH, Telnet, rlogin, et TCP brut."
createurices: "Simon Tatham"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "système"
    - "terminal"
    - "ssh"
lien_wikipedia: "https://fr.wikipedia.org/wiki/PuTTY"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/putty"
---

PuTTY est un émulateur de terminal sous Windows. Il permet également de se connecter via les protocoles SSH, Telnet, rlogin, et TCP brut ou encore d'établir des connexions directes par liaison série RS-232. À l'origine disponible uniquement pour Windows, il est possible de récupérer les sources pour Unix.

