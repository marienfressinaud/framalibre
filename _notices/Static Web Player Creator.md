---
nom: "Static Web Player Creator"
date_creation: "Lundi, 17 avril, 2017 - 14:46"
date_modification: "Lundi, 10 mai, 2021 - 14:13"
logo:
    src: "images/logo/Static Web Player Creator.png"
site_web: "http://www.webplayer.fr/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
description_courte: "Créer des fichiers web permettant d’écouter vos musiques."
createurices: "Matthieu Giroux"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "web"
    - "html5"
    - "téléchargement"
    - "audio"
    - "vidéo"
    - "édition"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/static-web-player-creator"
---

Static Web Player Creator permet de créer des fichiers web permettant d’écouter vos musiques avec un navigateur web. Les fichiers sont ajoutés au répertoire des musiques. Il peuvent être enlevés ensuite.

