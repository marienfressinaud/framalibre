---
nom: "Secure Drop"
date_creation: "Lundi, 4 décembre, 2017 - 14:04"
date_modification: "Lundi, 4 décembre, 2017 - 14:13"
logo:
    src: "images/logo/Secure Drop.png"
site_web: "https://securedrop.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Soumission confidentielle de documents à destination des journalistes."
createurices: "Aaron Swartz, Kevin Poulsen"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "sécurité"
    - "journalisme"
    - "presse"
    - "tor"
    - "chiffrement"
    - "communication chiffrée"
lien_wikipedia: "https://en.wikipedia.org/wiki/SecureDrop"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/secure-drop"
---

Secure Drop permet à des individus de transmettre des documents à des journalistes et de dialoguer avec eux de manière confidentielle et anonyme.
Le logiciel est à installer sur un serveur Ubuntu.
Une personne souhaitant envoyer des documents peut accéder à l'interface de soumission uniquement via Tor (service caché).  Un nom de code lui est attribué pour déposer les documents et pouvoir ensuite poursuivre les échanges. Les documents sont chiffrés avec la clé publique PGP d'un journaliste.
Les journalistes accèdent chacun à l'interface de consultation grâce à un service caché dédié, nécessitant une double authentification. Ils doivent utiliser Tails obligatoirement. Le déchiffrement et la consultation des documents se font uniquement sur une station de travail dédiée (déconnectée du réseau), grâce à leur clé PGP et un deuxième Tails.
Secure Drop est utilisé par des médias majeurs, comme par exemple The New York Times, The Guardian, The Intercept, The Washington Post, etc.

