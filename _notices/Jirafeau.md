---
nom: "Jirafeau"
date_creation: "Samedi, 15 avril, 2017 - 21:49"
date_modification: "Lundi, 10 mai, 2021 - 14:35"
logo:
    src: "images/logo/Jirafeau.jpeg"
site_web: "https://gitlab.com/mojo42/Jirafeau"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Partagez rapidement et simplement tous vos fichiers."
createurices: "Mojo"
alternative_a: "Imgur, ImageShack"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "partage"
    - "partage de fichiers"
    - "auto-hébergement"
    - "chiffrement"
    - "fichier"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/jirafeau"
---

Jirafeau offre la possibilité d'héberger et de partager vos fichiers, le tout en toute simplicité. Choisissez un fichier, Jirafeau vous fournira un lien avec beaucoup d'options.
Il est possible de protéger vos liens avec mot de passe ainsi que de choisir la durée de rétention du fichier sur le serveur. Le fichier et le lien s'autodétruiront passé ce délai.
Les téléchargements des fichiers transmis peuvent être limités à une certaine date, et chaque fichier peut s'autodétruire après le premier téléchargement.
Jirafeau permet de configurer les temps maximum de rétention ainsi que la taille maximale par fichier. Le chiffrement est disponible en option.

