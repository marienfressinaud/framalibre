---
nom: "CiviCRM"
date_creation: "Lundi, 4 mars, 2019 - 19:04"
date_modification: "Mardi, 11 mai, 2021 - 23:14"
logo:
    src: "images/logo/CiviCRM.png"
site_web: "https://civicrm.org"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "CiviCRM est un système de gestion des associations et des organisations sans but lucratif, conçu pour le web."
createurices: ""
alternative_a: "salesforce, nationbuilder, blackbaud, helloasso, sugarcrm"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métiers"
    - "crm"
    - "gestion de la relation client"
lien_wikipedia: "https://fr.wikipedia.org/wiki/CiviCRM"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/civicrm"
---

CiviCRM est un logiciel Web, libre, internationalisé, et spécialement conçu pour répondre aux besoins des groupes de pression, les organismes à but non-lucratif et les organisations non-gouvernementales. Son intégration aux systèmes de gestion de contenu Drupal, Joomla! et WordPress vous permet d'avoir une gamme d'outils pour connecter, communiquer et intéragir avec vos adhérents et constituants.
CiviCRM vous permet d'organiser, de capturer, traiter, analyser les informations relatives à vos constituants, d’automatiser et de synchroniser des taches administratives, de segmenter vos contacts de sorte que le bon message soit envoyé à la bonne personne au bon moment. La connaissance de chaque constituant à titre individuel est indispensable pour développer avec lui une relation durable.

