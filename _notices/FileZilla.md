---
nom: "FileZilla"
date_creation: "Mercredi, 11 février, 2015 - 00:07"
date_modification: "Lundi, 9 janvier, 2017 - 11:07"
logo:
    src: "images/logo/FileZilla.png"
site_web: "https://filezilla-project.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "FileZilla est un client FTP, FTPS et sFTP populaire."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "client ftp"
    - "ftp"
    - "transfert de fichiers"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FileZilla"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/filezilla"
---

FileZilla est un client FTP, FTPS et sFTP populaire. Il permet de se connecter en FTP sur les serveurs auxquels on a accès pour y télécharger ou y téléverser des fichiers.
Il gère le stockage des données de connexion de différents serveurs, le transfert de gros fichiers, les connexions multiples, les files d'attente, les limites de bandes passantes, etc.

