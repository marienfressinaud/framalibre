//@ts-check

import { readFile, writeFile } from 'fs/promises';

import { softwareNameToFileName, featuredSoftwares } from './commons.js'

import createNoticeContent from '../shared/createNoticeContent.js'

/*

à partir du dossier drupal-export, créer toutes les notices au format attendu

*/


const PLATEFORME_WEB = 'le web'
const WEB_TAG_CLOUD = 'cloud'
const WEB_TAG_WEBAPP = 'webapp'
const WEB_TAG_CLOUD_WEBAPP = 'cloud/webapps'



const AUTRES_LANGUES = 'Autres langues'
const LISTE_LANGUES = new Set(['Français', 'English', 'Español', AUTRES_LANGUES])

/**
 * 
 * @param {string} framalibreDrupalLanguesString 
 * @returns {string[]}
 */
function normalisationLangues(framalibreDrupalLanguesString){
    const langues = new Set(
        framalibreDrupalLanguesString
            .split(',')
            .map(s => s.trim())
            .filter(t => t.length >= 1)
    )

    for(const langue of langues){
        if(!LISTE_LANGUES.has(langue)){
            langues.delete(langue)
            langues.add(AUTRES_LANGUES)
        }
    }

    return [...langues]
}


const tagNormalisationMap = new Map([
    ['jeux', 'jeu'],
    ['calendrier', 'agenda'],
    ['mail', 'client mail'],
    ['fournisseur de messagerie électronique', 'client mail'],
    ['éditeur de texte', 'traitement de texte'],
])

/**
 * @param {{ [x: string]: string; }} notice
 * @returns {Set<string>}
 */
function createNoticeTagSet(notice){
    return new Set(
        [ notice["Catégorie"] ].concat( 
            notice["Étiquettes"]
                .split(',')
                .map(s => s.trim())
                .filter(t => t.length >= 1)
        )
        .map(t => t.toLowerCase())
        .map(tag => tagNormalisationMap.get(tag) || tag)
    )
}


readFile('./drupal-export/notices-export.json', { encoding: 'utf-8' })
.then(JSON.parse)
.then(exportedData => {
    console.log('exportedData', exportedData.length)

    /** @type {Map<any, Set<string>>} */
    const tagsByNotice = new Map();

    for(const notice of exportedData){
        tagsByNotice.set(notice, createNoticeTagSet(notice))
    }

    const countByTag = new Map();

    for(const tags of tagsByNotice.values()){
        for(const tag of tags){
            if(!countByTag.has(tag)){
                countByTag.set(tag, 1)
            }
            else{
                countByTag.set(tag, countByTag.get(tag) + 1)
            }
        }
    }

    const uniqueTags = new Set([...countByTag].filter(([tag, count]) => count === 1).map(([tag]) => tag))

    console.log('Nombre de tags', new Set([...countByTag.keys()]).size)

    console.log('Tags uniques trouvés', `(${uniqueTags.size})`, [...uniqueTags].join(', '))

    // suppression des tags uniques
    for(const tags of tagsByNotice.values()){
        for(const tag of tags){
            if(uniqueTags.has(tag)){
                tags.delete(tag)
            }
        }
    }


    return Promise.all(exportedData.map((/** @type {{ [x: string]: any; }} */ notice) => {
        /** @type {Set<string>} */
        // @ts-ignore
        const tags = tagsByNotice.get(notice)

        const nom = notice['Titre']    

        const langues = normalisationLangues(notice['Langue'])

        
        const plateformes = new Set(
            notice['Disponible sur']
            .split(',')
            // @ts-ignore
            .map(s => s.trim())
            // @ts-ignore
            .filter(s => s.length >= 1))

        if(tags.has(WEB_TAG_CLOUD) || tags.has(WEB_TAG_WEBAPP) || tags.has(WEB_TAG_CLOUD_WEBAPP)){
            tags.delete(WEB_TAG_CLOUD)
            tags.delete(WEB_TAG_WEBAPP)
            tags.delete(WEB_TAG_CLOUD_WEBAPP)
            plateformes.add(PLATEFORME_WEB)
        }

        const formattedNotice = createNoticeContent({
            nom,
            date_creation: notice['Post date'],
            date_modification: notice['Updated date'],
            logo: notice['Logo'],
            tags: tags,
            site_web: notice['Lien officiel'],
            plateformes: [...plateformes],
            langues,
            description_courte: notice['body_1'],
            createurices: notice['Créateur(s)'],
            alternative_a: notice['Alternative pour'],
            // @ts-ignore
            licences: notice['Licence(s)'].split(',').map(s => s.trim()).filter(s => s.length >= 1),
            lien_wikipedia: notice['Page Wikipédia'],
            lien_exodus: notice['Page Exodus Privacy'],
            identifiant_wikidata: '',
            description_longue: notice['Texte descriptif'],
            mis_en_avant: featuredSoftwares.includes(nom),
            redirect_from: notice['Chemin']
        });

        console.log('Création notice', softwareNameToFileName(nom, '.md'))

        return writeFile(`./_notices/${softwareNameToFileName(nom, '.md')}`, formattedNotice, 'utf-8')
    }))

})


