---
layout: form_confirmation
confirmation_message: |
  La modification a été envoyée à l'équipe de Framalibre qui la
  validera dans les jours à venir.
---
